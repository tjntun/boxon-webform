jQuery(document).ready(function($) {
    $(function() {
        $(".datepicker-input").datepicker();
    });


    // 
    $('#price-request-mondi [name=reset]').click(function(event) {
    	event.preventDefault();
    	showPopUp('Do you really want to reset the form?',function () {
    		$('#price-request-mondi input, #price-request-mondi select').not('[type=submit], [type=reset]').val("");
    	});
    });
    function showPopUp(title,action){
        $("#popUpModel").modal({show:true});
        $('#ModelLabel').text(title);

        if (action != undefined) {
        	$('#makeChanges').click(function(event) {
        		action();
        		$("#popUpModel").modal('hide');
        	});
        }
    }

    
});
