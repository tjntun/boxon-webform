<div class="container">

    <div class="starter-template">
        <h1 class="text-center"><?php echo ucwords(str_replace('_', ' ', $formType)); ?></h1>
    </div>

    <a href="<?php echo site_url('/forms/add'); ?>" class="btn btn-default">Back</a>
    <a href="<?php echo $formType .'/add'; ?>" class="btn btn-info">Add New Form</a>
    <br>
    <br>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th class="text-center">View detail</th>
            <th>Created</th>
            <th>Last modified</th>
            <th class="text-center">Download PDF</th>
            <th class="text-center">Email Me</th>
            <th class="text-center">Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(!empty($forms)){
            foreach($forms as $form){ ?>
        <tr>
            <td><?php echo $form['ID'] ?></td>
            
            <td class="text-center"><a href="<?php echo site_url('/'.$form['FormType'].'/edit/'.$form['ID']) ?>" class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></a></td>
            <td><?php echo $form['Created'] ?></td>
            <td><?php echo $form['Modified'] ?></td>
            <td class="text-center"><a href="<?php echo site_url('/'.$form['FormType'].'/generatePdf/'.$form['ID']) ?>" class="btn btn-default"><i class="glyphicon glyphicon glyphicon-download-alt"></i></a></td>
            <td class="text-center"><a href="<?php echo site_url('/'.$form['FormType'].'/email/'.$form['ID']) ?>" class="btn btn-default"><i class="glyphicon glyphicon-envelope"></i></a></td>
            <td class="text-center"><a href="<?php echo site_url('/'.$formType.'/delete/'.$form['ID']) ?>" class="btn btn-default del_form"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
        <?php
            }//end foreach
        }//end if
        ?>
        </tbody>
    </table>

    <?php echo $this->pagination->create_links(); ?>
</div>

<!-- Modal -->
<div class="modal fade" id="popUpModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="ModelLabel">Modal title</h4>
            </div>
            <div class="modal-body" id="modelContent">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="#" class="btn btn-primary" id="makeChanges">Save changes</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.del_form').on('click', function(e){
            e.preventDefault();
            showPopUp("Delete Users",'Are you sure want to Delete this User',$(this).attr('href'));
        });

        function showPopUp(title,content,url) {
            $("#popUpModel").modal({show:true});
            $('#ModelLabel').text(title);
            $("#modelContent").text(content);
            $("#makeChanges").attr('href',url);
        }
    })
</script>
