<div class="container">

    <div class="starter-template">
        <h1 class="text-center">Send Form with PDF</h1>
    </div>

    <hr/>

    <form class="form-horizontal" role="form" method="post" action="<?php echo current_url(); ?>">
        <?php echo validation_errors(); ?>
        <div class="form-group">
            <label for="emails" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" value="<?php echo set_value('emails') ?>" name="emails" id="emails" placeholder="eg: abc@abc.com, xyz@xyz.com">
                <span class="help-block">Comma Separated Email ID'S</span>
            </div>
        </div>
        <div class="form-group">
            <label for="subject" class="col-sm-2 control-label">Subject</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" value="<?php echo set_value('subject') ?>" name="subject" id="subject" placeholder="Subject">
            </div>
        </div>
        <div class="form-group">
            <label for="message" class="col-sm-2 control-label">Optional Message</label>
            <div class="col-sm-10">
                <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Message"><?php echo set_value('message') ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" value="Send" name="submit" class="btn btn-default"/>
            </div>
        </div>
    </form>
</div>