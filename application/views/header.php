<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $this->config->item('SITE_TITLE') ?></title>

    <link rel="stylesheet" href="<?php echo site_url('/assets/css/bootstrap.min.css') ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('/assets/css/jquery-ui.css') ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('/assets/css/style.css') ?>"/>

    <style>
        body { padding-top: 110px; }
        .logo{
            position: absolute;
            left: 50%;
            margin-left: -84px;
            top: 20px;
        }
    </style>

    <script type="text/javascript" src="<?php echo site_url('/assets/js/jquery-1.11.1.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('/assets/js/jquery-ui.js') ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('/assets/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('/assets/js/jquery-calx-1.1.9.js') ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('/assets/js/main.js') ?>"></script>


</head>
<body>

<!-- <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?php //echo $this->config->item('SITE_TITLE') ?></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php //echo site_url('/home/') ?>">Home</a></li>
                <li><a href="<?php //echo site_url('/forms/') ?>">Forms</a></li>
            </ul>
        </div>
    </div>
</div> -->

<div class="container">
    <?php $messages = $this->session->flashdata('message'); ?>
    <?php if($messages) : ?>
        <?php foreach($messages as $key => $value) : ?>
            <div class="alert alert-<?php echo $key; ?>">
                <?php echo $value; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<img src="<?php echo site_url('/assets/images/logo.png'); ?>" class="logo">
<?php if($this->user->user != null): ?>
<a href="<?php echo site_url('/home/logout') ?>" class="btn btn-default" id="logout-btn">Logout</a>
<?php endif; ?>

<?php if($this->user->user != null): ?>
<a href="<?php echo site_url('/home/my_info') ?>" class="btn btn-default" id="myinfo-btn">My info</a>
<?php endif; ?>