<div class="container">

    <div class="starter-template">
        <h1 class="text-center">User list</h1>
    </div>

    <div class="btn-group">
        <a href="<?php echo site_url('/admin/dashboard/add') ?>" class="btn btn-info">Add User</a>
        <button type="button" class="btn btn-danger delete_all">Delete</button>

        <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Change Status
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" class="status" rel="active">Active</a></li>
                <li><a href="#" class="status" rel="inactive">InActive</a></li>
            </ul>
        </div>
    </div>

    <hr/>
    <table class="table">
        <thead>
        <tr>   
            <th><input type="checkbox" name="" class="select_all" id=""/></th>
            <th>#</th>
            <th>First Name</th>
            <th>Email</th>
            <th>Last login</th>
            <th>Enabled</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($users as $user) : ?>
        <tr>
            <td><input type="checkbox" name="ID[]" class="checkall" value="<?php echo $user['ID'] ?>"/></td>
            <td><?php echo $user['ID'] ?></td>
            <td><?php echo $user['FullName'] ?></td>
            <td><?php echo $user['Email'] ?></td>
            <td><?php echo $user['LastLogin'] ?></td>
            <td>
                <input class="user-status" data-id="<?php echo $user['ID']; ?>" type="checkbox" <?php if($user['Status']=='active') echo 'checked="checked"'; ?>>
            </td>

            <td><a href="<?php echo site_url('/admin/dashboard/edit/'.$user['ID']) ?>" class="btn"><i class="glyphicon glyphicon-edit"></i></a></td>
            <td><a href="<?php echo site_url('/admin/dashboard/delete/'.$user['ID']) ?>" class="btn del_user"><i class="glyphicon glyphicon-remove"></i></a></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


    <div class="btn-group">
        <button type="button" class="btn btn-info">Add User</button>
        <button type="button" class="btn btn-danger delete_all">Delete</button>

        <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                Change Status
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" class="status" rel="active">Active</a></li>
                <li><a href="#" class="status" rel="inactive">InActive</a></li>
            </ul>
        </div>
    </div>

    <br/>
    <hr/>

    <?php echo $this->pagination->create_links(); ?>


</div>


<!-- Modal -->
<div class="modal fade" id="popUpModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="ModelLabel">Modal title</h4>
            </div>
            <div class="modal-body" id="modelContent">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="#" class="btn btn-primary" id="makeChanges">Save changes</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.status').on('click', function(e){
            e.preventDefault();

            var serialize = $(".checkall:checked").serializeArray();
            var ids = [],counter = 0;
            for(key in serialize){
                ids[counter] = serialize[key]['value'];
                counter++;
            }
            ids = ids.join('-');
            var action = $(this).attr('rel');
            if(ids == ''){
                showPopUp("Change status",'You have not select any thing','');
                $("#makeChanges").hide();

            } else {
                showPopUp("Change status",'Are you sure want to change status','<?php echo site_url('/admin/dashboard/change_status') ?>/'+action+'/'+ids)
                $("#makeChanges").show();
            }
        });

        $('.user-status').click(function(event) {
            var action = 'inactive';
            if ($(this).is(':checked')) {
                action = 'active';
            }
            var ids = $(this).data('id');
            showPopUp("Change status",'Are you sure want to change status','<?php echo site_url('/admin/dashboard/change_status') ?>/'+action+'/'+ids)
        });

        $('.select_all').on('change',function() {
            if($(this).is(':checked')) {
                $('.checkall').attr('checked', 'true');
            } else {
                $('.checkall').removeAttr('checked');
            }
        });


        $(".delete_all").on('click', function(e){
            e.preventDefault();

            var serialize = $(".checkall:checked").serializeArray();
            var ids = [],counter = 0;
            for(key in serialize){
                ids[counter] = serialize[key]['value'];
                counter++;
            }
            ids = ids.join('-');

            if(ids == ''){
                showPopUp("Delete Users",'You have not select any thing','');
                $("#makeChanges").hide();

            } else {
                showPopUp("Delete Users",'Are you sure want to Delete Users','<?php echo site_url('/admin/dashboard/delete') ?>/'+ids)
                $("#makeChanges").show();
            }


        });


        $('.del_user').on('click', function(e){
            e.preventDefault();

            showPopUp("Delete Users",'Are you sure want to Delete this User',$(this).attr('href'));
            $("#makeChanges").show();

        });



        function showPopUp(title,content,url){
            $("#popUpModel").modal({show:true});
            $('#ModelLabel').text(title);
            $("#modelContent").text(content);
            $("#makeChanges").attr('href',url);
        }
    })
</script>
