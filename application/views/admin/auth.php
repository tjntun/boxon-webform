<div class="container">

    <form class="form-signin center" method="post" action="<?php echo site_url('admin/auth/index') ?>" autocomplete="off">
        <label for="user_email">Admin Username</label>
        <input type="text" class="form-control" name="user_email" value="<?php echo set_value('user_email') ?>" placeholder="" required="" autofocus="">
        <br>
        <label for="user_pass">Admin Password</label>
        <input type="password" class="form-control" name="user_pass" value="<?php echo set_value('user_pass') ?>" placeholder="" required="">
        <input name="redirect_uri" value="<?php echo base64_encode(current_url()); ?>" type="hidden"/>
        <button class="btn btn-lg btn-primary btn-block" name="submit" value="submit" type="submit">Sign in</button>
    </form>

</div><!-- /.container -->