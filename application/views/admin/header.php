<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $this->config->item('SITE_TITLE') ?></title>

    <link rel="stylesheet" href="<?php echo site_url('/assets/css/bootstrap.min.css') ?>"/>
    <link rel="stylesheet" href="<?php echo site_url('/assets/css/style.css') ?>"/>

    <script type="text/javascript" src="<?php echo site_url('/assets/js/jquery-1.11.1.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('/assets/js/bootstrap.min.js') ?>"></script>

</head>
<body>
<!-- <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Admin Panel</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php //echo site_url('/admin/dashboard/') ?>">Home</a></li>
            </ul>
        </div>
    </div>
</div> -->

<div class="container">
    <?php $messages = $this->session->flashdata('message'); ?>
    <?php if($messages) : ?>
        <?php foreach($messages as $key => $value) : ?>
            <div class="alert alert-<?php echo $key; ?>">
                <?php echo $value; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<?php if($this->user->admin != null): ?>
<a href="<?php echo site_url('/admin/dashboard/logout') ?>" class="btn btn-default" id="logout-btn">Logout</a>
<?php endif; ?>
<img src="<?php echo site_url('/assets/images/logo.png'); ?>" class="logo">