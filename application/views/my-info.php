<div class="container">
    <div class="starter-template">
        <h1 class="text-center">My info</h1>
    </div>
    <?php echo validation_errors(); ?>
    <form role="form" class="user-form" method="post" action="<?php echo current_url(); ?>">
        <div class="form-group">
            <label for="FullName" class="control-label">Full Name</label>
            <input type="text" class="form-control" value="<?php echo set_value('FullName',$this->template->setFieldValue('FullName')) ?>" id="FullName" name="FullName" placeholder="">
        </div>
        <div class="form-group">
            <label for="Email" class="control-label">Email</label>
            <input type="text" class="form-control" id="Email" name="Email" value="<?php echo set_value('Email',$this->template->setFieldValue('Email')) ?>" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="Password" class="control-label">Password</label>
            <input type="password" class="form-control" id="Password" name="Password" placeholder="Password">
            <?php if($this->uri->segment(3) == 'edit') : ?>
            <span class="help-block">If you dont want to change the password then leave it blank</span>
            <?php endif ?>
        </div>
        <div class="form-group">
            <label for="PasswordConfirm" class="control-label">Confirm Password</label>
             <input type="password" class="form-control" id="PasswordConfirm" name="PasswordConfirm" placeholder="Confirm Password">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-info" value="Save" name="submit">
            <input type="button" class="btn btn-defaul" value="Cancel" name="cancel">
        </div>
    </form>
</div>
<script>
    jQuery(document).ready(function($) {
        $('input[name=cancel]').click(function(event) {
            location.href = '<?php echo site_url("/") ?>';
        });
    });
</script>