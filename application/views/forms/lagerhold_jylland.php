<?php 
    // Helper function
    function getNameFromNumber($num) {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return getNameFromNumber($num2) . $letter;
        } else {
            return $letter;
        }
    }
?>

<div class="container">

    <div class="row">
        <h2 class="text-center">
            Lagerhold Jylland
        </h2>
    </div>
    <hr>
    <form id="lagerhold-jylland-form" method="POST" action="<?php echo current_url() ?>" class="boxon-webforms form-horizontal text-left" role="form">
        <h3 class="text-center">JIT-kalkule med lager</h3>
        <div class="form-group"  style="width:500px; margin:20px auto;">
            <label for="postnumber" class="control-label col-sm-6">Kundes postnummer</label>
            <div class="col-sm-6">
                <?php
                $kundesPostnumber = array(
                    "10-36" => "10-36",
                    "37" => "37",
                    "40-47" => "40-47",
                    "48-49" => "48-49",
                    "50-59" => "50-59",
                    "60,66.70-71" => "60,66.70-71",
                    "61-65" => "61-65",
                    "67-69, 72-74" => "67-69, 72-74",
                    "75-79" => "75-79",
                    "85, 88-89" => "85, 88-89",
                    "80-84, 86-87" => "80-84, 86-87",
                    "90-92, 5" => "90-92, 5",
                    "93-95, 97-99" => "93-95, 97-99",
                );

                echo form_dropdown('kundes-postnumber', $kundesPostnumber, set_value('kundes-postnumber',$this->template->setFieldValue('kundes-postnumber')), 'class="form-control"');
                echo form_error('kundes-postnumber');
                ?>
            </div>
        </div>
        <table class="table" id="tab1">
            <tr>
                <?php for($i = 1; $i<=33; $i++): ?>
                <th><?php echo getNameFromNumber($i); ?></th>
                <?php endfor; ?>
            </tr>
            <tr class="tableizer-firstrow">
                <th></th>
                <th>Volumen</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Paller</th>
                <th>EUR</th>
                <th>Stykpris</th>
                <th>&nbsp;</th>
                <th>Omsh.</th>
                <th>Årlig leje</th>
                <th>Leje</th>
                <th>Kost, palle</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Salg/Adm.</th>
                <th>Omk. tillæg</th>
                <th>Vareværdi</th>
                <th>Tillæg</th>
                <th>Stykpris</th>
                <th>Årlig</th>
                <th>Total årlig</th>
                <th>TG 1</th>
                <th>SP</th>
                <th>Utvärde</th>
                <th>TG 1</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Vælg</th>
                <th>Salgspris</th>
                <th>Vælg</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <th>Reference</th>
                <th>Årlig</th>
                <th>Pr. indkøb</th>
                <th>Min. lager</th>
                <th>m2/palle</th>
                <th>Antal/palle</th>
                <th>fullload</th>
                <th>1000 styk</th>
                <th>inkl. fragt</th>
                <th>Gns. lager</th>
                <th>lager</th>
                <th>pr. palle</th>
                <th>pr. palle</th>
                <th>handling</th>
                <th>Rente</th>
                <th>Fragt</th>
                <th>omk.</th>
                <th>pr. palle</th>
                <th>pr. palle</th>
                <th>%</th>
                <th>i alt</th>
                <th>vareværdi</th>
                <th>vareværdi</th>
                <th>kalkyl</th>
                <th>Direkte</th>
                <th>manuellt</th>
                <th>Manuellt</th>
                <th>&nbsp;</th>
                <th>Best punkt</th>
                <th>DG2</th>
                <th>pr. styk</th>
                <th>salgspris</th>
                <th>DG2</th>
            </tr>

            <?php for($i = 1; $i <= 2; $i++):
                $j = 1; 
            ?>
            <tr data-rowtab1="<?php echo $i; ?>">
                <!-- A --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- B --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- C --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- D --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" data-formula="ROUND($tab1B<?php echo $i; ?>/50*4)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- E --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php if($action == 'edit'): ?>
                <!-- F --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- G --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php else: ?>
                <!-- F --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,'1') ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- G --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,'1') ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php endif; ?>
                <!-- H --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" value="0" data-format="0.00" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- I --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0.00" data-formula="($tab1H<?php echo $i; ?>*7.5/1000)+(9300/$tab1G<?php echo $i; ?>/$tab1F<?php echo $i; ?>)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- J --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="FLOOR($tab1D<?php echo $i; ?>+0.5*$tab1C<?php echo $i; ?>)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- K --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" data-formula="$tab1B<?php echo $i; ?>/$tab1J<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- L --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="365*($tab1E<?php echo $i; ?>*1)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- M --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" data-formula="$tab1L<?php echo $i; ?>/$tab1K<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- N --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text" value="30"></td>
                <!-- O --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="0*($tab1S<?php echo $i; ?>/$tab1K<?php echo $i; ?>)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- P --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Q --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="(0+0)*$tab1S<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- R --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="SUM($tab1M<?php echo $i; ?>,$tab1Q<?php echo $i; ?>)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- S --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab1F<?php echo $i; ?>*$tab1I<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- T --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab1R<?php echo $i; ?>/$tab1S<?php echo $i; ?>" data-format="0.0%" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- U --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab1I<?php echo $i; ?>+($tab1I<?php echo $i; ?>*$tab1T<?php echo $i; ?>)" data-format="0.00" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- V --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" data-formula="$tab1B<?php echo $i; ?>*$tab1I<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- W --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab1B<?php echo $i; ?>*$tab1U<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- X --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="ROUND(($tab1U<?php echo $i; ?>-$tab1I<?php echo $i; ?>)/$tab1U<?php echo $i; ?>)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Y --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="1" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Z --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab1B<?php echo $i; ?>*$tab1Y<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AA --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="ROUND(($tab1Y<?php echo $i; ?>-$tab1I<?php echo $i; ?>)/$tab1Y<?php echo $i; ?>)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AB --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab1B<?php echo $i; ?>/50*4" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AC --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab1D<?php echo $i; ?>+$tab1AB<?php echo $i; ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AD --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" value="0" data-formula="" value="23" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AE --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="IF($tab1H<?php echo $i; ?><1,0,(ROUND($tab1U<?php echo $i; ?>/(1-$tab1AD<?php echo $i; ?>/100))))" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AF --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab1-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab1-'. getNameFromNumber($j) . $i)) ?>" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AG --><td><input name="tab1-<?php echo getNameFromNumber($j) . $i; ?>" data-format="0.0%" data-formula="IF($tab1H<?php echo $i; ?><1,0,($tab1AF<?php echo $i; ?>-$tab1U<?php echo $i; ?>)/$tab1AF<?php echo $i; ?>)" id="tab1<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
            </tr>
            <?php endfor;//end for ?>

        </table>

        <hr>
        <h3 class="text-center">Levering via KH</h3>


        <table class="table" id="tab2">
            <tr>
                <?php for($i = 1; $i<=33; $i++): ?>
                <th><?php echo getNameFromNumber($i); ?></th>
                <?php endfor; ?>
            </tr>
            <tr class="tableizer-firstrow">
                <th></th>
                <th>Volumen</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Paller</th>
                <th>EUR</th>
                <th>Stykpris</th>
                <th>&nbsp;</th>
                <th>Omsh.</th>
                <th>Årlig leje</th>
                <th>Leje</th>
                <th>Kost, palle</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Salg/Adm.</th>
                <th>Omk. tillæg</th>
                <th>Vareværdi</th>
                <th>Tillæg</th>
                <th>Stykpris</th>
                <th>Årlig</th>
                <th>Total årlig</th>
                <th>TG 1</th>
                <th>SP</th>
                <th>Utvärde</th>
                <th>TG 1</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Vælg</th>
                <th>Salgspris</th>
                <th>Vælg</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <th>Reference</th>
                <th>Årlig</th>
                <th>Pr. indkøb</th>
                <th>Min. lager</th>
                <th>m2/palle</th>
                <th>Antal/palle</th>
                <th>fullload</th>
                <th>1000 styk</th>
                <th>inkl. fragt</th>
                <th>Gns. lager</th>
                <th>lager</th>
                <th>pr. palle</th>
                <th>pr. palle</th>
                <th>handling</th>
                <th>Rente</th>
                <th>Fragt</th>
                <th>omk.</th>
                <th>pr. palle</th>
                <th>pr. palle</th>
                <th>%</th>
                <th>i alt</th>
                <th>vareværdi</th>
                <th>vareværdi</th>
                <th>kalkyl</th>
                <th>Direkte</th>
                <th>manuellt</th>
                <th>Manuellt</th>
                <th>&nbsp;</th>
                <th>Best punkt</th>
                <th>DG2</th>
                <th>pr. styk</th>
                <th>salgspris</th>
                <th>DG2</th>
            </tr>

            <?php for($i = 1; $i <= 2; $i++):
                $j = 1; 
            ?>
            <tr data-row="<?php echo $i; ?>">
                <!-- A --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- B --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- C --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- D --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" data-formula="ROUND($B<?php echo $i; ?>/50*4)" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- E --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php if($action == 'edit'): ?>
                <!-- F --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- G --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php else: ?>
                <!-- F --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,'1') ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- G --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,'1') ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php endif; ?>
                <!-- H --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0.00" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- I --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0.00" data-formula="($H<?php echo $i; ?>*7.5/1000)+(9300/$G<?php echo $i; ?>/$F<?php echo $i; ?>)" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- J --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="FLOOR($D<?php echo $i; ?>+0.5*$C<?php echo $i; ?>)" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- K --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" data-formula="$B<?php echo $i; ?>/$J<?php echo $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- L --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" value="0" type="text"></td>
                <!-- M --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- N --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text" value="30"></td>
                <!-- O --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- P --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Q --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="(0+0)*$S<?php echo $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- R --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="SUM($M<?php echo $i; ?>,$Q<?php echo $i; ?>)" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- S --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$F<?php echo $i; ?>*$I<?php echo $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- T --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$R<?php echo $i; ?>/$S<?php echo $i; ?>" data-format="0.0%" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- U --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$I<?php echo $i; ?>+($I<?php echo $i; ?>*$T<?php echo $i; ?>)" data-format="0.00" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- V --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" data-formula="$B<?php echo $i; ?>*$I<?php echo $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- W --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$B<?php echo $i; ?>*$U<?php echo $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- X --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="ROUND(($U<?php echo $i; ?>-$I<?php echo $i; ?>)/$U<?php echo $i; ?>)" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Y --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="1" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Z --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$B<?php echo $i; ?>*$Y<?php echo $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AA --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="ROUND(($Y<?php echo $i; ?>-$I<?php echo $i; ?>)/$Y<?php echo $i; ?>)" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AB --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AC --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$D<?php echo $i; ?>+$AB<?php echo $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AD --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="" value="23" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AE --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="IF($H<?php echo $i; ?><1,0,(ROUND($U<?php echo $i; ?>/(1-$AD<?php echo $i; ?>/100))))" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AF --><td><input value="<?php echo set_value('tab2-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab2-'. getNameFromNumber($j) . $i)) ?>" name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AG --><td><input name="tab2-<?php echo getNameFromNumber($j) . $i; ?>" data-format="0.0%" data-formula="IF($H<?php echo $i; ?><1,0,($AF<?php echo $i; ?>-$U<?php echo $i; ?>)/$AF<?php echo $i; ?>)" id="<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
            </tr>
            <?php endfor;//end for ?>

        </table>
        
        <hr>
        
        <h3 class="text-center">Direkte</h3>
        
        <!-- Tab 3 -->
        <table class="table" id="tab3">
            <tr>
                <?php for($i = 1; $i<=33; $i++): ?>
                <th><?php echo getNameFromNumber($i); ?></th>
                <?php endfor; ?>
            </tr>
            <tr class="tableizer-firstrow">
                <th></th>
                <th>Volumen</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Paller</th>
                <th>EUR</th>
                <th>Stykpris</th>
                <th>&nbsp;</th>
                <th>Omsh.</th>
                <th>Årlig leje</th>
                <th>Leje</th>
                <th>Kost, palle</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Salg/Adm.</th>
                <th>Omk. tillæg</th>
                <th>Vareværdi</th>
                <th>Tillæg</th>
                <th>Stykpris</th>
                <th>Årlig</th>
                <th>Total årlig</th>
                <th>TG 1</th>
                <th>SP</th>
                <th>Utvärde</th>
                <th>TG 1</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Vælg</th>
                <th>Salgspris</th>
                <th>Vælg</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <th>Reference</th>
                <th>Årlig</th>
                <th>Pr. indkøb</th>
                <th>Min. lager</th>
                <th>m2/palle</th>
                <th>Antal/palle</th>
                <th>fullload</th>
                <th>1000 styk</th>
                <th>inkl. fragt</th>
                <th>Gns. lager</th>
                <th>lager</th>
                <th>pr. palle</th>
                <th>pr. palle</th>
                <th>handling</th>
                <th>Rente</th>
                <th>Fragt</th>
                <th>omk.</th>
                <th>pr. palle</th>
                <th>pr. palle</th>
                <th>%</th>
                <th>i alt</th>
                <th>vareværdi</th>
                <th>vareværdi</th>
                <th>kalkyl</th>
                <th>Direkte</th>
                <th>manuellt</th>
                <th>Manuellt</th>
                <th>&nbsp;</th>
                <th>Best punkt</th>
                <th>DG2</th>
                <th>pr. styk</th>
                <th>salgspris</th>
                <th>DG2</th>
            </tr>

            <?php for($i = 1; $i <= 2; $i++):
                $j = 1; 
            ?>
            <tr data-row="<?php echo $i; ?>">
                <!-- A --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- B --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- C --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- D --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" data-formula="ROUND($tab3B<?php echo $i; ?>/50*4)" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- E --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php if($action == 'edit'): ?>
                <!-- F --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- G --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php else: ?>
                <!-- F --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,'1') ?>" name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- G --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,'1') ?>" name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <?php endif; ?>
                <!-- H --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0.00" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- I --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="1" data-format="0.00" data-formula="($tab3H<?php echo $i; ?>*7.5/1000)+(9300/$tab3G<?php echo $i; ?>/$tab3F<?php echo $i; ?>)" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- J --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="FLOOR($tab3D<?php echo $i; ?>+0.5*$tab3C<?php echo $i; ?>)" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- K --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" data-formula="$tab3B<?php echo $i; ?>/$tab3J<?php echo $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- L --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" value="0" type="text"></td>
                <!-- M --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- N --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text" value="30"></td>
                <!-- O --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- P --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Q --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="(0+0)*$tab3S<?php echo $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- R --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="SUM($tab3M<?php echo $i; ?>,$tab3Q<?php echo $i; ?>)" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- S --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab3F<?php echo $i; ?>*$tab3I<?php echo $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- T --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab3R<?php echo $i; ?>/$tab3S<?php echo $i; ?>" data-format="0.0%" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- U --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab3I<?php echo $i; ?>+($tab3I<?php echo $i; ?>*$tab3T<?php echo $i; ?>)" data-format="0.00" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- V --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-format="0" data-formula="$tab3B<?php echo $i; ?>*$tab3I<?php echo $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- W --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab3B<?php echo $i; ?>*$tab3U<?php echo $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- X --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="ROUND(($tab3U<?php echo $i; ?>-$tab3I<?php echo $i; ?>)/$tab3U<?php echo $i; ?>)" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Y --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="1" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
                <!-- Z --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab3B<?php echo $i; ?>*$tab3Y<?php echo $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AA --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="ROUND(($tab3Y<?php echo $i; ?>-$tab3I<?php echo $i; ?>)/$tab3Y<?php echo $i; ?>)" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AB --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AC --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="$tab3D<?php echo $i; ?>+$tab3AB<?php echo $i; ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AD --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" value="0" data-formula="" value="23" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AE --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="0" data-formula="IF($tab3H<?php echo $i; ?><1,0,(ROUND($tab3U<?php echo $i; ?>/(1-$tab3AD<?php echo $i; ?>/100))))" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AF --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" value="<?php echo set_value('tab3-'. getNameFromNumber($j) . $i,$this->template->setFieldValue('tab3-'. getNameFromNumber($j) . $i)) ?>" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
               <!-- AG --><td><input name="tab3-<?php echo getNameFromNumber($j) . $i; ?>" data-format="0.0%" data-formula="IF($tab3H<?php echo $i; ?><1,0,($tab3AF<?php echo $i; ?>-$tab3U<?php echo $i; ?>)/$tab3AF<?php echo $i; ?>)" id="tab3<?php echo getNameFromNumber($j++) . $i; ?>" type="text"></td>
            </tr>
            <?php endfor;//end for ?>

        </table>

        <div class="row fixed-form-footer">
            <div class="center">
                <input type="submit" style="margin-right: 15px;" value="Save" id="btn_submit" class="btn btn-success" name="submit"/>
                <button type="reset" style="margin-right: 15px;" class="btn btn-default" name="reset">Reset</button>
                <?php if (isset($action) && $action == 'edit'): ?>
                <a href="#" class="btn btn-default" style="margin-right: 15px;" id="print-btn">Print</a>
                <a href="<?php echo site_url('/'.$formType.'/email/'.$form['ID']) ?>" class="btn btn-default" style="margin-right: 15px;" >Email me</a>
                <?php endif; ?>
                <a href="<?php echo site_url('/'.$formType) ?>" class="btn btn-default">Back</a>
            </div>
        </div>


    </form>
</div>
<script>
    jQuery(document).ready(function($) {
        $('#lagerhold-jylland-form').calx();
        
        $('#print-btn').click(function(event) {
            $('.fixed-form-footer, #myinfo-btn, #logout-btn').hide();
            window.print();
            $('.fixed-form-footer, #myinfo-btn, #logout-btn').show();
        });

        $("form :input").on('keyup', function(){
            $("#btn_submit").prop('disabled', false);
            
        });

        // $('input').keyup(function(event) {
        //     $('#lagerhold-jylland-form').each(function(index, el) {
        //         this.reset();
        //     }).calx('refresh');
        //     alert('content');
        // });

        $("form input[type='checkbox'],form select").on('change', function(){
            $("#btn_submit").prop('disabled', false)
        })

        $("#btn_submit").prop('disabled', true);
        $('hr').width($('#tab1').width());
    });
</script>
