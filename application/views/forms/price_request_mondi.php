<div class="container">
<h1 class="text-center">Price Request - Mondi</h1>

<hr/>

<form id="price-request-mondi" method="POST" action="<?php echo current_url(); ?>" class="form-horizontal text-left boxon-webforms">

<h3 class="text-left">
    Boxon Reference
</h3>

<div class="form-group">
    <label for="requestdate" class="col-md-3">
        Request Date:
    </label>

    <div class="col-md-3">

        <div class="input-group date datepicker no-padding" data-date-format="dd-mm-yyyy" id="requestdate">
            <input type='text' class="form-control datepicker-input" name="requestdate" value="<?php echo set_value('requestdate',$this->template->setFieldValue('requestdate')) ?>"/>
             <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
        </div>
        <?php echo form_error('requestdate') ?>
    </div>

    <label for="prreference" class="col-md-3">
        PR. Ref:
    </label>

    <div class="col-md-3">
        <input type="text" class="form-control" name="prreference" id="prreference" placeholder="Enter PR. Reference"
               value="<?php echo set_value('prreference',$this->template->setFieldValue('prreference')) ?>">
        <?php echo form_error('prreference'); ?>
    </div>
</div>

<hr>

<div class="form-group">
    <label for="reference" class="col-md-3">
        Reference:
    </label>

    <div class="col-md-9">
        <input type="text" class="form-control" name="reference" id="reference" placeholder="Enter Reference"
               value="<?php echo set_value('reference',$this->template->setFieldValue('reference')) ?>">
        <?php echo form_error('reference'); ?>
    </div>
</div>


<div class="form-group">
    <label for="fefco" class="col-md-3">
        FEFCO:
    </label>

    <div class="col-md-3">
        <?php
        $fefco = array(
            '' => '--Please Select--','0200' => '0200','0201' => '0201','0202' => '0202','0203' => '0203','0204' => '0204','0205' => '0205','0206' => '0206','0207' => '0207','0208' => '0208','0209' => '0209','0211' => '0211','0212' => '0212','0214' => '0214','0215' => '0215','0216' => '0216','0217' => '0217','0218' => '0218','0225' => '0225','0226' => '0226','0300' => '0300','0301' => '0301','0302' => '0302','0303' => '0303','0304' => '0304','0305' => '0305','0306' => '0306','0307' => '0307','0310' => '0310','0311' => '0311','0312' => '0312','0313' => '0313','0320' => '0320','0321' => '0321','0322' => '0322','0323' => '0323','0325' => '0325','0330' => '0330','0401' => '0401','0402' => '0402','0403' => '0403','0404' => '0404','0405' => '0405','0406' => '0406','0407' => '0407','0409' => '0409','0410' => '0410','0411' => '0411','0412' => '0412','0413' => '0413','0415' => '0415','0416' => '0416','0420' => '0420','0421' => '0421','0422' => '0422','0423' => '0423','0424' => '0424','0425' => '0425','0426' => '0426','0427' => '0427','0428' => '0428','0429' => '0429','0430' => '0430','0431' => '0431','0432' => '0432','0433' => '0433','0434' => '0434','0435' => '0435','0436' => '0436','0437' => '0437','0440' => '0440','0441' => '0441','0442' => '0442','0443' => '0443','0444' => '0444','0445' => '0445','0446' => '0446','0447' => '0447','0450' => '0450','0451' => '0451','0452' => '0452','0453' => '0453','0454' => '0454','0455' => '0455','0456' => '0456','0457' => '0457','0458' => '0458','0459' => '0459','0460' => '0460','0470' => '0470','0501' => '0501','0502' => '0502','0503' => '0503','0504' => '0504','0505' => '0505','0507' => '0507','0508' => '0508','0509' => '0509','0601' => '0601','0602' => '0602','0605' => '0605','0606' => '0606','0607' => '0607','0608' => '0608','0610' => '0610','0615' => '0615','0616' => '0616','0620' => '0620','0621' => '0621','0700' => '0700','0701' => '0701','0703' => '0703','0711' => '0711','0712' => '0712','0713' => '0713','0714' => '0714','0715' => '0715','0716' => '0716','0717' => '0717','0718' => '0718','0747' => '0747','0748' => '0748','0751' => '0751','0752' => '0752','0759' => '0759','0760' => '0760','0761' => '0761','0770' => '0770','0771' => '0771','0772' => '0772','0773' => '0773','0774' => '0774','0900' => '0900','0901' => '0901','0902' => '0902','0903' => '0903','0904' => '0904','0905' => '0905','0906' => '0906','0907' => '0907','0908' => '0908','0909' => '0909','0910' => '0910','0911' => '0911','0913' => '0913','0914' => '0914','0920' => '0920','0921' => '0921','0929' => '0929','0930' => '0930','0931' => '0931','0932' => '0932','0933' => '0933','0934' => '0934','0935' => '0935','0940' => '0940','0941' => '0941','0942' => '0942','0943' => '0943','0944' => '0944','0945' => '0945','0946' => '0946','0947' => '0947','0948' => '0948','0949' => '0949','0950' => '0950','0951' => '0951','0965' => '0965','0966' => '0966','0967' => '0967','0970' => '0970','0971' => '0971','0972' => '0972','0973' => '0973','0974' => '0974','0975' => '0975','0976' => '0976',
        );

        echo form_dropdown('fefco', $fefco, set_value('fefco',$this->template->setFieldValue('fefco')), 'class="form-control"');
        echo form_error('fefco');
        ?>
    </div>


    <label for="quantity" class="col-md-3">
        Quantity:
    </label>

    <div class="col-md-3">
        <?php
        $quantity = array('' => '--Please Select--', 
            'E_11T3T3_B/B_TL3-120_WB-105_TL3-120___4300' => 'E_11T3T3_B/B_TL3-120_WB-105_TL3-120___4300',
            'E_11TWT3_1_W/B _TL3-120_WB-105_TW-125___4200' => 'E_11TWT3_1_W/B _TL3-120_WB-105_TW-125___4200',
            'E_11TWT3_2_W/B_TL3-135_WB 105_TW-140___4002' => 'E_11TWT3_2_W/B_TL3-135_WB 105_TW-140___4002',
            'E_14T3T3_B/B_TL3-170_WB-135_TL3-170___5000' => 'E_14T3T3_B/B_TL3-170_WB-135_TL3-170___5000',
            'E_11TWTW_W/B_TW-125_WB-105_TW-125___4300' => 'E_11TWTW_W/B_TW-125_WB-105_TW-125___4300',
            'E_12KWKW_W/W_KW-140_WB-105_KW-140___5400' => 'E_12KWKW_W/W_KW-140_WB-105_KW-140___5400',
            'E_14KWT3_W/B_TL3-170_WB-135_KW-140___5800' => 'E_14KWT3_W/B_TL3-170_WB-135_KW-140___5800',
            'B_11T3T3_1_B/B_TL3-120_WB-105_TL3-120___4950' => 'B_11T3T3_1_B/B_TL3-120_WB-105_TL3-120___4950',
            'B_14T3T3_2_B/B_TL3-135_WB-135_TL3-170___5500' => 'B_14T3T3_2_B/B_TL3-135_WB-135_TL3-170___5500',
            'B_12KST3_3_B/B_TL3-135_WB-120_KS-135___4800' => 'B_12KST3_3_B/B_TL3-135_WB-120_KS-135___4800',
            'B_14TWT3_3_W/B_TL3-170_WB-135_TW-170___5450' => 'B_14TWT3_3_W/B_TL3-170_WB-135_TW-170___5450',
            'B_14TWT3_1_W/B_T3-135_WB-135_TW-140___5011' => 'B_14TWT3_1_W/B_T3-135_WB-135_TW-140___5011',
            'B_11T3T3_2_B/B_T3-135_WB-105_TL3-135___4022' => 'B_11T3T3_2_B/B_T3-135_WB-105_TL3-135___4022',
            'B_16TWKW_W/W_TW-170_WB-120_KW-140___6000' => 'B_16TWKW_W/W_TW-170_WB-120_KW-140___6000',
            'B_11TWTW_1_W/W_TW-125_WB-105_TW-140___5112' => 'B_11TWTW_1_W/W_TW-125_WB-105_TW-140___5112',
            'B_18KWKBS_W/B_KB-186_SC-135_KW-140___7300' => 'B_18KWKBS_W/B_KB-186_SC-135_KW-140___7300',
            'C_12T3T3_2_B/B_TL3-135_WB-120_TL3-135___5150' => 'C_12T3T3_2_B/B_TL3-135_WB-120_TL3-135___5150',
            'C_12TWT3_W/B_TL3-120_WB-120_TW-140___4500' => 'C_12TWT3_W/B_TL3-120_WB-120_TW-140___4500',
            'C_11T3T3_1_B/B_TL3-120_WB-105_TL3-120___5000' => 'C_11T3T3_1_B/B_TL3-120_WB-105_TL3-120___5000',
            'C_14T3T3_4_B/B_TL3-170_WB-135_TL3-170___5600' => 'C_14T3T3_4_B/B_TL3-170_WB-135_TL3-170___5600',
            'C_14TWT3_3_W/B_TL3-170_WB-135_TW-170___5600' => 'C_14TWT3_3_W/B_TL3-170_WB-135_TW-170___5600',
            'C_14KST3_2_B/B_TL3-170_WB-135_KS-170___5850' => 'C_14KST3_2_B/B_TL3-170_WB-135_KS-170___5850',
            'C_18KBKBS1_B/B_KL-186_SC-135_KL-186___7800' => 'C_18KBKBS1_B/B_KL-186_SC-135_KL-186___7800',
            'C_18KWKBS_W/B_KB-186_SC-135_KW-140___7450' => 'C_18KWKBS_W/B_KB-186_SC-135_KW-140___7450',
            'EB20T3T3_2_B/B_TL3-135_WB-120_WB-105_WB-120_TL3-135_8400' => 'EB20T3T3_2_B/B_TL3-135_WB-120_WB-105_WB-120_TL3-135_8400',
            'EB20T3T3_3_B/B_TL3-120_WB-105_WB-105_WB-105_TL3-120_8000' => 'EB20T3T3_3_B/B_TL3-120_WB-105_WB-105_WB-105_TL3-120_8000',
            'EB20TWT3_1_W/B_TL3-120_WB-105_WB-105_WB-105_TW-120_8000' => 'EB20TWT3_1_W/B_TL3-120_WB-105_WB-105_WB-105_TW-120_8000',
            'EB_24TWKBS_W/B_KB-186_SC 140_WB-105_SC-140_TW-170_12000' => 'EB_24TWKBS_W/B_KB-186_SC 140_WB-105_SC-140_TW-170_12000',
            'BC_20T3T3_1_B/B_TL3-120_WB-105_WB-105_WB-105_TL3-120_7000' => 'BC_20T3T3_1_B/B_TL3-120_WB-105_WB-105_WB-105_TL3-120_7000',
            'BC_20T3T3_3_B/B_TL3-170_WB-120_WB-105_WB-105_TL3-170_10500' => 'BC_20T3T3_3_B/B_TL3-170_WB-120_WB-105_WB-105_TL3-170_10500',
            'BC_20TWT3_1_W/B_TL3-120_WB-105_WB-105_WB-120_TW-140_7000' => 'BC_20TWT3_1_W/B_TL3-120_WB-105_WB-105_WB-120_TW-140_7000',
            'BC_22T3T3_B/B_TL3-170_WB-135_WB-120_WB-135_TL3-170_11500' => 'BC_22T3T3_B/B_TL3-170_WB-135_WB-120_WB-135_TL3-170_11500',
            'BC_20KBT3_B/B_TL3-120_WB-105_WB-105_WB-105_KB-135_8200' => 'BC_20KBT3_B/B_TL3-120_WB-105_WB-105_WB-105_KB-135_8200',
            'BC24KBKBS1_B/B_KL-186_SC-125_WB-105_SC-125_KL-300_13500' => 'BC24KBKBS1_B/B_KL-186_SC-125_WB-105_SC-125_KL-300_13500',
            'BC22TWKB_S_W/B_KB-135_SC-125_WB-120_SC-125_TW-140_10200' => 'BC22TWKB_S_W/B_KB-135_SC-125_WB-120_SC-125_TW-140_10200',
            'BC20TWT3_1_W/B_TL3-120_WB-105_WB-105_WB-120_TW-140_9000' => 'BC20TWT3_1_W/B_TL3-120_WB-105_WB-105_WB-120_TW-140_9000',
        );
        echo form_dropdown('quantity', $quantity, set_value('quantity',$this->template->setFieldValue('quantity')), 'class="form-control"');
        echo form_error('quantity');
        ?>
    </div>


</div>

<div class="form-group">
    <label for="insidedimensions" class="col-md-3">
        Inside Dimensions:
    </label>

    <div class="col-md-2">
        <input type="text" class="form-control" name="length" id="length" placeholder="Length"
               value="<?php echo set_value('length',$this->template->setFieldValue('length')) ?>">
        <?php echo form_error('length'); ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="width" id="width" value="<?php echo set_value('width',$this->template->setFieldValue('width')) ?>"
               placeholder="Width">
        <?php echo form_error('width'); ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" value="<?php echo set_value('height',$this->template->setFieldValue('height')) ?>" name="height" id="height"
               placeholder="Height">
        <?php echo form_error('height'); ?>
    </div>

</div>


<div class="form-group">
    <label for="print" class="col-md-3">
        Print:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('print', array('' => '--Please Select--', 'No' => 'No', '1 Color' => '1 Color', '2 Color' => '2 Color', '3 Color' => '3 Color', '4 Color' => '4 Color', '5 Color' => '5 Color'), set_value('print',$this->template->setFieldValue('print')), 'class="form-control"') ?>
        <?php echo form_error('print') ?>
    </div>


    <label for="coverage" class="col-md-3">
        Coverage:
    </label>

    <div class="col-md-3">
        <?php
        $coverage = array('0' => '--Please Select--', 5 => '5', 10 => '10', 15 => '15', 20 => '20', 25 => '25', 30 => '30', 35 => '35', 40 => '40', 45 => '45', 50 => '50', 55 => '55', 60 => '60', 65 => '65', 70 => '70', 75 => '75', 80 => '80', 85 => '85', 90 => '90', 95 => '95', 100 => '100');
        echo form_dropdown('coverage', $coverage, set_value('coverage',$this->template->setFieldValue('coverage')), 'class="form-control"');
        echo form_error('coverage');
        ?>
    </div>


</div>

<div class="form-group">
    <label for="assembling" class="col-md-3">
        Assembling:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('assembling', array('' => '--Please Select--', 'None' => 'None', 'Glued' => 'Glued', 'Stiched' => 'Stiched'), set_value('assembling',$this->template->setFieldValue('assembling')), 'class="form-control"') ?>
        <?php echo form_error('assembling') ?>
    </div>
</div>


<div class="form-group">
    <label for="quantity" class="col-md-3">
        Quantity:
    </label>

    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity1" value="<?php echo set_value('quantity1',$this->template->setFieldValue('quantity1')) ?>"
               id="quantity1" placeholder="Quantity">
        <?php echo form_error('quantity1') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity2" value="<?php echo set_value('quantity2',$this->template->setFieldValue('quantity2')) ?>"
               id="quantity2" placeholder="Quantity">
        <?php echo form_error('quantity2') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity3" value="<?php echo set_value('quantity3',$this->template->setFieldValue('quantity3')) ?>" id="quantity3"
               placeholder="Quantity">
        <?php echo form_error('quantity3') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity4" value="<?php echo set_value('quantity4',$this->template->setFieldValue('quantity4')) ?>" id="quantity4"
               placeholder="Quantity">
        <?php echo form_error('quantity4') ?>
    </div>
</div>

<div class="form-group">
    <label for="bundled" class="col-md-3">
        Bundled:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('bundled', array('' => '--Please Select--', 'Yes' => 'Yes', 'No' => 'No'), set_value('bundled',$this->template->setFieldValue('bundled')), 'class="form-control"') ?>
        <?php echo form_error('bundled') ?>
    </div>

    <label for="peicesbundle" class="col-md-3">
        Peices Per Bundle:
    </label>

    <div class="col-md-3">
        <input type="text" class="form-control" name="peicesbundle" value="<?php echo set_value('peicesbundle',$this->template->setFieldValue('peicesbundle')) ?>"
               id="peicesbundle" placeholder="Enter Bundle Pieces">
        <?php echo form_error('peicesbundle') ?>
    </div>
</div>


<div class="form-group">
    <label for="strecthfilmonpallets" class="col-md-3">
        Strecth Film On Pallets:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('strecthfilmonpallets', array('' => '--Please Select--', 'Yes' => 'Yes', 'No' => 'No'), set_value('strecthfilmonpallets',$this->template->setFieldValue('strecthfilmonpallets')), 'class="form-control"') ?>
        <?php echo form_error('strecthfilmonpallets') ?>
    </div>
</div>

<div class="form-group">
    <label for="quantityperpallet" class="col-md-3">
        Quantity Per Pallet:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('quantityperpallet', array('' => '--Please Select--', 'Fixed' => 'Fixed', 'Optimized' => 'Optimized'), set_value('quantityperpallet',$this->template->setFieldValue('quantityperpallet')), 'class="form-control"') ?>
        <?php echo form_error('quantityperpallet') ?>
    </div>

    <div class="col-md-3">
        <input type="text" class="form-control" name="peicesbundle" value="<?php echo set_value('peicesbundle',$this->template->setFieldValue('peicesbundle')) ?>" id="peicesbundle" placeholder="Quantity Per Pallet">
        <?php echo form_error('peicesbundle') ?>
    </div>

</div>

<div class="form-group">
    <label for="maxpalletdimension" class="col-md-3">
        Max Pallet Dimension:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('maxpalletdimension', array('' => '--Please Select--', 'Yes' => 'Yes', 'No' => 'No'), set_value('maxpalletdimension',$this->template->setFieldValue('maxpalletdimension')), 'class="form-control"') ?>
        <?php echo form_error('maxpalletdimension') ?>
    </div>
</div>

<div class="form-group">
    <label for="dimension" class="col-md-3">
        Dimension:
    </label>

    <div class="col-md-2">
        <input type="text" class="form-control" name="length1" value="<?php echo set_value('length1',$this->template->setFieldValue('length1')) ?>" id="length1" placeholder="Length">
        <?php echo form_error('length1') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="width1" id="width1"
               placeholder="Width" value="<?php echo set_value('width1',$this->template->setFieldValue('width1')) ?>">
        <?php echo form_error('width1') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="height1" id="height1" placeholder="Height" value="<?php echo set_value('height1',$this->template->setFieldValue('height1')) ?>">
        <?php echo form_error('height1') ?>
    </div>
</div>


<div class="form-group">
    <label for="pallet" class="col-md-3">
        Pallet:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('pallet', array('' => '--Please Select--', 'One Way' => 'One Way', 'EUR' => 'EUR', 'New EUR' => 'New EUR'), set_value('pallet',$this->template->setFieldValue('pallet')), 'class="form-control"') ?>
        <?php echo form_error('pallet') ?>
    </div>
</div>
<div class="row fixed-form-footer">
    <div class="center">
        <input type="submit" style="margin-right: 15px;" value="Save" id="btn_submit" class="btn btn-success" name="submit"/>
        <button type="reset" style="margin-right: 15px;" class="btn btn-default" name="reset">Reset</button>
        <?php if (isset($action) && $action == 'edit'): ?>
        <a href="#" class="btn btn-default" style="margin-right: 15px;" id="print-btn">Print</a>
        <a href="<?php echo site_url('/price_request_mondi/email/'.$form['ID']) ?>" class="btn btn-default" style="margin-right: 15px;" >Email me</a>
        <?php endif; ?>
        <a href="<?php echo site_url('/price_request_mondi') ?>" class="btn btn-default">Back</a>
    </div>
</div>
</form>


</div>
<?php if($this->uri->segment(2) == 'edit') : ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#print-btn').click(function(event) {
                $('.fixed-form-footer, #myinfo-btn, #logout-btn').hide();
                window.print();
                $('.fixed-form-footer, #myinfo-btn, #logout-btn').show();
            });

            $("form :input").on('keyup', function(){
                $("#btn_submit").prop('disabled', false)
            });

            $("form input[type='checkbox'],form select").on('change', function(){
                $("#btn_submit").prop('disabled', false)
            })

            $("#btn_submit").prop('disabled', true)
        })
    </script>
<?php endif; ?>