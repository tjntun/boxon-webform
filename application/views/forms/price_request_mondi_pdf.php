<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Price Request form - Mondi</title>


    <link rel="stylesheet" href="<?php echo site_url('/assets/css/grid.css') ?>"/>

</head>
<body>
<div class="container">
    <h1 class="text-center">Price Request form - Werner Kenkel</h1>

    <hr/>
    <h3 class="text-left">
        Boxon Reference
    </h3>
    
    <table class="table table-bordered">
        <tr>
            <td width="25%">Request Date: </td>
            <td><b><?php echo $this->template->formData['requestdate'] ?></b></td>
            <td>PR. Reference: </td>
            <td colspan="2"><b><?php echo $this->template->formData['prreference'] ?></b></td>
        </tr>
        <tr>
            <td colspan="1">Reference:</td>
            <td colspan="4">
                <b><?php echo $this->template->formData['reference'] ?></b>
            </td>
        </tr>
    </table>

    <table width="98%" class="table table-bordered">
        <tr>
            <td>FEFCO:</td>
            <td><b><?php echo $this->template->formData['fefco']; ?></b></td>
            <td>Quantity:</td>
            <td colspan="2"><b><?php echo $this->template->formData['quantity']; ?></b></td>
        </tr>

        <tr>
            <td>Inside Dimensions:</td>
            <td>Length : <b><?php echo $this->template->formData['length']; ?></b></td>
            <td>Width : <b><?php echo $this->template->formData['width']; ?></b> </td>
            <td colspan="2">Height : <b><?php echo $this->template->formData['height']; ?></b></td>

        </tr>

        <tr>
            <td> Print: </td>
            <td><b><?php echo $this->template->formData['print']; ?></b></td>
            <td> Coverage: </td>
            <td colspan="2"><b><?php echo $this->template->formData['coverage']; ?></b></td>
        </tr>

        <tr>
            <td>
                Assembling:
            </td>
            <td colspan="4"><b><?php echo $this->template->formData['assembling']; ?></b></td>
        </tr>

        <tr>
            <td>
                Quantity:
            </td>
            <td>One : <b><?php echo $this->template->formData['quantity1']; ?></b></td>
            <td>Two : <b><?php echo $this->template->formData['quantity2']; ?></b></td>
            <td>Three : <b><?php echo $this->template->formData['quantity3']; ?></b></td>
            <td>Four : <b><?php echo $this->template->formData['quantity4']; ?></b></td>
        </tr>

        <tr>
            <td>Bundled:</td>
            <td><b><?php echo $this->template->formData['bundled']; ?></b></td>
            <td>Peices Per Bundle:</td>
            <td colspan="2"><b><?php echo $this->template->formData['peicesbundle']; ?></b></td>
        </tr>

        <tr>
            <td>
                Strecth Film On Pallets:
            </td>
            <td colspan="4"><b><?php echo $this->template->formData['strecthfilmonpallets']; ?></b></td>
        </tr>

        <tr>
            <td>Quantity Per Pallet: </td>
            <td><b><?php echo $this->template->formData['quantityperpallet']; ?></b></td>
            <td colspan="3"><b><?php echo $this->template->formData['peicesbundle']; ?></b></td>
        </tr>

        <tr>
            <td>
                Max Pallet Dimension: >
            </td>
            <td colspan="4"><b><?php echo $this->template->formData['maxpalletdimension']; ?></b</td>
        </tr>

        <tr>
            <td>
                Dimension: Length :
            </td>
            <td>Length : <b><?php echo $this->template->formData['length1']; ?></b></td>
            <td>Width : <b><?php echo $this->template->formData['width1']; ?></b></td>
            <td colspan="2">Height : <b><?php echo $this->template->formData['height1']; ?></b></td>
        </tr>

        <tr>
            <td>
                Pallet:
            </td>
            <td colspan="4"><b><?php echo $this->template->formData['pallet']; ?></b></td>
        </tr>
    </table>


</div>

</body>
</html>