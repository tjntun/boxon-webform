<?php 
    // Helper function
    function getNameFromNumber($num) {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return getNameFromNumber($num2) . $letter;
        } else {
            return $letter;
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
            
    <title>Lagerhold Sjaeland</title>

    <link rel="stylesheet" href="<?php echo site_url('/assets/css/grid.css') ?>"/>

</head>
<body>
<div class="container">
    <h1 class="text-center"></h1>
        Lagerhold Sjaeland
    <hr/>
    
    <h3 class="text-center">
        Kundes postnumber: <?php echo $this->template->formData['kundes-postnumber'] ?>
    </h3>
    <?php for($tabIdx = 1; $tabIdx <= 3; $tabIdx++): ?>
    <table class="table table-bordered">
        <tr class="tableizer-firstrow">
            <th></th>
            <th>Volumen</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>Paller</th>
            <th>EUR</th>
            <th>Stykpris</th>
            <th>&nbsp;</th>
            <th>Omsh.</th>
            <th>Årlig leje</th>
            <th>Leje</th>
            <th>Kost, palle</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>Salg/Adm.</th>
            <th>Omk. tillæg</th>
            <th>Vareværdi</th>
            <th>Tillæg</th>
            <th>Stykpris</th>
            <th>Årlig</th>
            <th>Total årlig</th>
            <th>TG 1</th>
            <th>SP</th>
            <th>Utvärde</th>
            <th>TG 1</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>Vælg</th>
            <th>Salgspris</th>
            <th>Vælg</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>Reference</th>
            <th>Årlig</th>
            <th>Pr. indkøb</th>
            <th>Min. lager</th>
            <th>m2/palle</th>
            <th>Antal/palle</th>
            <th>fullload</th>
            <th>1000 styk</th>
            <th>inkl. fragt</th>
            <th>Gns. lager</th>
            <th>lager</th>
            <th>pr. palle</th>
            <th>pr. palle</th>
            <th>handling</th>
            <th>Rente</th>
            <th>Fragt</th>
            <th>omk.</th>
            <th>pr. palle</th>
            <th>pr. palle</th>
            <th>%</th>
            <th>i alt</th>
            <th>vareværdi</th>
            <th>vareværdi</th>
            <th>kalkyl</th>
            <th>Direkte</th>
            <th>manuellt</th>
            <th>Manuellt</th>
            <th>&nbsp;</th>
            <th>Best punkt</th>
            <th>DG2</th>
            <th>pr. styk</th>
            <th>salgspris</th>
            <th>DG2</th>
        </tr>

        <?php for($i = 1; $i <= 2; $i++):
                $j = 1; 
            ?>
        <tr data-row="<?php echo $i; ?>">
            <!-- A --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- B --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- C --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- D --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- E --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- F --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- G --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- H --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- I --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- J --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- K --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- L --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- M --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- N --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- O --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- P --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- Q --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- R --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- S --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- T --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- U --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- V --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- W --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- X --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- Y --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
            <!-- Z --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
           <!-- AA --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
           <!-- AB --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
           <!-- AC --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
           <!-- AD --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
           <!-- AE --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
           <!-- AF --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
           <!-- AG --><td><?php echo $this->template->formData['tab'. $tabIdx .'-' . getNameFromNumber($j++) . $i]; ?></td>
        </tr>
        <?php endfor;//end for ?>
    </table>
    <?php endfor; ?>
</div>

</body>
</html>