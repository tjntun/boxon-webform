<div class="container">
<div class="wrap">

<div class="row">
    <h2 class="text-center">
        Prisberegning FEFCO 0201
    </h2>
</div>


<form id="pricerequest" method="POST" action="<?php current_url() ?>" class="form-horizontal text-left">
<div class="form-group">
    <label for="reference" class="col-md-3">
        Reference:
    </label>
    <div class="col-md-9">
        <input type="text" class="form-control" name="reference" id="reference" value="<?php echo set_value('reference', $this->template->setFieldValue('reference')) ?>" placeholder="Enter Reference">
        <?php echo form_error('reference'); ?>
    </div>
</div>


<div class="form-group">
    <label for="quality" class="col-md-3">
        Kvalitet :
    </label>
    <div class="col-md-3">
        <?php
            $quality = array ( '' => '--Please Select--', '0201' => '0201', '0202' => '0202', '0203' => '0203', '0204' => '0204', '0205' => '0205', '0206' => '0206', '0207' => '0207', '0208' => '0208', '0209' => '0209', '0211' => '0211', '0212' => '0212', '0214' => '0214', '0215' => '0215', '0216' => '0216', '0217' => '0217', '0218' => '0218', '0225' => '0225', '0226' => '0226', '0230' => '0230');
            echo form_dropdown('quality',$quality,set_value('quality',$this->template->setFieldValue('quality')),'class="form-control"');
            echo form_error('quality');
        ?>
    </div>

</div>



<div class="form-group">
    <label for="insidedimensions" class="col-md-3">
        Indvendige mal :
    </label>
    <div class="col-md-2">
        <input type="text" class="form-control" name="length" id="length" placeholder="Length" value="<?php echo set_value('length',$this->template->setFieldValue('length')) ?>">
        <?php echo form_error('length') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="width" id="width" placeholder="Width" value="<?php echo set_value('width',$this->template->setFieldValue('width')) ?>">
        <?php echo form_error('width') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="height" id="height" placeholder="Height" value="<?php echo set_value('height',$this->template->setFieldValue('height')) ?>">
        <?php echo form_error('height') ?>
    </div>

</div>


<div class="form-group">
    <label for="warehouse" class="col-md-3">
        Oplag :
    </label>
    <div class="col-md-3">
        <input type="text" class="form-control" name="warehouse" id="warehouse" placeholder="Quantity" value="<?php echo set_value('warehouse',$this->template->setFieldValue('warehouse')) ?>">
        <?php echo form_error('warehouse') ?>
    </div>

    <label for="totalm2" class="col-md-3">
        Total m2:
    </label>
    <div class="col-md-3">
        <input type="text" class="form-control" name="totalm2" id="totalm2" placeholder="Total m2" value="<?php echo set_value('totalm2',$this->template->setFieldValue('totalm2')) ?>">
        <?php echo form_error('totalm2') ?>
    </div>

</div>

<div class="form-group">
    <label for="requestaccepted" class="col-md-offset-6 col-md-4">
        Foresporgsel Accepteret:
    </label>
    <div class="col-md-2">
        <?php echo form_dropdown('requestaccepted',array('Yes' => 'Yes','No' => 'No'),set_value('requestaccepted',$this->template->setFieldValue('requestaccepted')),'class="form-control"'); ?>
        <?php echo form_error('requestaccepted') ?>
    </div>
</div>


</br>
</br>

<div class="row clearfix">


</div>



<div class="row clearfix">

    <div class="col-md-offset-3 col-md-9 text-center">
        <h4> Ordre iDKK </h4>
    </div>

</div>


<div class="row clearfix">
    <div class="col-md-offset-3 col-md-9 text-center">


        <div class="col-md-6 text-center" >
            <p> Kostpris  (Cost) </p>

            <div class="col-md-6 text-center">
                <p> Per Stock </p>
            </div>

            <div class="col-md-6 text-center">
                <p> Total </p>
            </div>

        </div>


        <div class="col-md-6 text-center" >
            <p> Salgspris  (Sales Price) </p>

            <div class="col-md-6 text-center">
                <p> Per Stock </p>
            </div>

            <div class="col-md-6 text-center">
                <p> Total </p>
            </div>

        </div>

    </div>
</div>



<div class="row clearfix">
    <div class="col-md-offset-3 col-md-9 text-center">

        <div class="col-md-6 text-center">

            <div class="col-md-6">
                <input type="text" class="form-control" name="costqty" id="costqty" placeholder="Quantity" value="<?php echo set_value('costqty',$this->template->setFieldValue('costqty')) ?>">
                <?php echo form_error('costqty') ?>
            </div>

            <div class="col-md-6">
                <input type="text" class="form-control" name="costtotal" id="costtotal" placeholder="Total" value="<?php echo set_value('costqty',$this->template->setFieldValue('costtotal')) ?>">
                <?php echo form_error('costtotal') ?>
            </div>

        </div>


        <div class="col-md-6 text-center">

            <div class="col-md-6">
                <input type="text" class="form-control" name="spqty" id="spqty" placeholder="Quantity" value="<?php echo set_value('spqty',$this->template->setFieldValue('spqty')) ?>">
                <?php echo form_error('spqty') ?>
            </div>

            <div class="col-md-6">
                <input type="text" class="form-control" name="sptotal" id="sptotal" placeholder="Total" value="<?php echo set_value('sptotal',$this->template->setFieldValue('sptotal')) ?>">
                <?php echo form_error('sptotal') ?>
            </div>

        </div>

    </div>
</div>




</br>

<div class="row clearfix">
    <div class="col-md-offset-3 col-md-9 text-center">

        <div class="col-md-6 col-sm-6 text-center" >
            <div <div class="col-md-offset-6 col-md-6 col-sm-6 text-center">
                <p> DG </p>
            </div>
        </div>


        <div class="col-md-6 col-sm-6 text-center" >
            <div class="col-md-6 col-sm-6 text-center">
                <p> Avance  </p>
            </div>
        </div>

    </div>
</div>



<div class="row clearfix">

    <div class="col-md-offset-3 col-md-9 text-center">
        <div class="col-md-6 text-center">
            <div class="col-md-offset-6 col-md-6">
                <input type="text" class="form-control" name="percentage" id="percentage" placeholder="Percentage" value="<?php echo set_value('percentage',$this->template->setFieldValue('percentage')) ?>">
                <?php echo form_error('percentage') ?>
            </div>

        </div>


        <div class="col-md-6 text-center">
            <div class="col-md-6">
                <input type="text" class="form-control" name="profitqty" id="profitqty" placeholder="Quantity" value="<?php echo set_value('profitqty',$this->template->setFieldValue('profitqty')) ?>">
                <?php echo form_error('profitqty') ?>
            </div>

            <div class="col-md-6">
                <input type="text" class="form-control" name="profittotal" id="profittotal" placeholder="Total" value="<?php echo set_value('profittotal',$this->template->setFieldValue('profittotal')) ?>">
                <?php echo form_error('profittotal') ?>
            </div>
        </div>
    </div>
</div>





</br>
</br>


<div class="row">

    <div class="col-md-3">
    </div>

    <div class="col-md-6">
        <input type="submit"  name="submit" value="Submit" id="btn_submit" class="btn btn-success"/>
        <button type="reset" class="btn btn-danger" name="reset">Reset</button>
    </div>


</div>
</form>


<?php if($this->uri->segment(3) == 'edit') : ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $("form :input").on('keyup', function(){
                $("#btn_submit").prop('disabled', false)
            });

            $("form input[type='checkbox'],form select").on('change', function(){
                $("#btn_submit").prop('disabled', false)
            })

            $("#btn_submit").prop('disabled', true)

        })
    </script>
<?php endif; ?>

</br>
</br>

</div>
</div> <!-- End of container -->