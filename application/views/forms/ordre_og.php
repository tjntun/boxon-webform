<div class="container">
    <div class="wrap">


        <div class="row">
            <h2 class="text-center">
                ORDRE- OG FORESPØRGSELSFORMULAR
            </h2>
        </div>

        <form action="<?php echo current_url(); ?>" class="form-horizontal" method="post" id="mein_form">
            <h3>Kundedetaljer</h3>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td colspan="2">
                            <div class="form-group col-lg-12">
                                <label for="firmanavn">Firmanavn:</label>
                                <input type="text" class="form-control" id="firmanavn" placeholder="Firmanavn" name="firmanavn" value="<?php echo set_value('firmanavn',$this->template->setFieldValue('firmanavn')) ?>">
                                <?php echo form_error('firmanavn') ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group col-lg-12">
                                <label for="CVR_nr">CVR nr:</label>
                                <input type="text" class="form-control" id="CVR_nr" placeholder="CVR Nr" name="CVR_nr" value="<?php echo set_value('CVR_nr',$this->template->setFieldValue('CVR_nr')) ?>">
                                <?php echo form_error('CVR_nr') ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group col-lg-12">
                                <label for="saelger">Sælger:</label>
                                <input type="text" class="form-control" id="saelger" placeholder="saelger" name="saelger" value="<?php echo set_value('saelger',$this->template->setFieldValue('saelger')) ?>">
                                <?php echo form_error('saelger') ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group col-lg-12">
                                <label for="saelger">Kontaktperson:</label>
                                <input type="text" class="form-control" id="kontaktperson" placeholder="Kontaktperson" name="kontaktperson" value="<?php echo set_value('kontaktperson',$this->template->setFieldValue('kontaktperson')) ?>">
                                <?php echo form_error('kontaktperson') ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group col-lg-12">
                                <label for="saelger">Mail:</label>
                                <input type="text" class="form-control" id="mail" placeholder="Mail" name="mail" value="<?php echo set_value('mail',$this->template->setFieldValue('mail')) ?>">
                                <?php echo form_error('mail') ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group col-lg-12">
                                <label for="saelger">Telefonnummer:</label>
                                <input type="text" class="form-control" id="telefonnummer" placeholder="Telefonnummer" name="telefonnummer" value="<?php echo set_value('telefonnummer',$this->template->setFieldValue('telefonnummer')) ?>">
                                <?php echo form_error('telefonnummer') ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group col-lg-12">
                                <label for="saelger">Fakturamail:</label>
                                <input type="text" class="form-control" id="fakturamail" placeholder="Fakturamail:" name="fakturamail" value="<?php echo set_value('fakturamail',$this->template->setFieldValue('fakturamail')) ?>">
                                <?php echo form_error('fakturamail') ?>
                            </div>

                        </td>

                    </tr>

                    <tr>
                        <td colspan="2">
                            <div class="form-group col-lg-12">
                                <label for="faktureringsadresse">Faktureringsadresse:</label>
                                <input type="text" class="form-control" id="faktureringsadresse" placeholder="Faktureringsadresse:" name="faktureringsadresse" value="<?php echo set_value('faktureringsadresse',$this->template->setFieldValue('faktureringsadresse')) ?>">
                                <?php echo form_error('faktureringsadresse') ?>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="form-group col-lg-12">
                                <label for="leveringsadresse">Leveringsadresse:</label>
                                <input type="text" class="form-control" id="leveringsadresse" placeholder="Leveringsadresse:" name="leveringsadresse" value="<?php echo set_value('leveringsadresse',$this->template->setFieldValue('leveringsadresse')) ?>">
                                <?php echo form_error('leveringsadresse') ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="table-responsive">
                        <h3>Opgavedetaljer</h3>


                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="dato">Dato:</label>
                                        <input type="text" class="form-control" id="dato" placeholder="Dato:" name="dato" value="<?php echo set_value('dato',$this->template->setFieldValue('dato')) ?>">
                                        <?php echo form_error('dato') ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="leveringsdato">Leveringsdato:</label>
                                        <input type="text" class="form-control" id="leveringsdato" placeholder="Leveringsdato" name="leveringsdato" value="<?php echo set_value('leveringsdato',$this->template->setFieldValue('leveringsdato')) ?>">
                                        <?php echo form_error('leveringsdato') ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="forespørgsel">Forespørgsel:</label>
                                        <input type="checkbox" <?php echo $this->template->checked('foresporgsel') ?> id="forespørgsel" placeholder="Forespørgsel" name="foresporgsel" value="YES">

                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="ordre">Ordre:</label>
                                        <input type="checkbox" id="ordre" placeholder="Ordre" <?php echo $this->template->checked('ordre') ?> name="ordre" value="YES">

                                    </div>
                                </td>
                            </tr>

                        </table>

                        <table class="table table-bordered">
                            <tr>
                                <td colspan="2">
                                    <div class="form-group col-lg-12">
                                        <label for="id_ref">ID/ref.:</label>
                                        <input type="text" class="form-control" id="id_ref" placeholder="ID/ref" name="id_ref" value="<?php echo set_value('id_ref',$this->template->setFieldValue('id_ref')) ?>">
                                        <?php echo form_error('id_ref') ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="fefco">Fefco:</label>
                                        <input type="text" class="form-control" id="fefco" placeholder="Fefco" name="fefco" value="<?php echo set_value('fefco',$this->template->setFieldValue('fefco')) ?>">
                                        <?php echo form_error('fefco') ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="Prove_vedlagt">Prøve vedlagt:</label>
                                        <input type="text" class="form-control" id="Prove_vedlagt" placeholder="Prøve vedlagt" name="Prove_vedlagt" value="<?php echo set_value('Prove_vedlagt',$this->template->setFieldValue('Prove_vedlagt')) ?>">
                                        <?php echo form_error('Prove_vedlagt') ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="kvalitet">Kvalitet:</label>
                                        <input type="text" class="form-control" id="kvalitet" placeholder="Prøve vedlagt" name="kvalitet" value="<?php echo set_value('kvalitet',$this->template->setFieldValue('kvalitet')) ?>">
                                        <?php echo form_error('kvalitet') ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="flute">Flute:</label>
                                        <input type="text" class="form-control" id="flute" placeholder="Flute" name="flute" value="<?php echo set_value('flute',$this->template->setFieldValue('flute')) ?>">
                                        <?php echo form_error('flute') ?>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div class="form-group col-lg-12">
                                        <label for="mal">Mal:</label>
                                        <input type="text" class="form-control" id="mal" placeholder="Mal" name="mal" value="<?php echo set_value('mal',$this->template->setFieldValue('mal')) ?>">
                                        <?php echo form_error('mal') ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="form-group col-lg-12">
                                        <label for="mal">Hjørnelukning:</label>

                                        <table class="table">
                                            <tr>
                                                <td style="border-top: 0;">Lim <input type="checkbox" <?php echo $this->template->checked('lim') ?> name="lim" value="YES" id="lim"/></td>
                                                <td style="border-top: 0;">3 pkt. <input type="checkbox" <?php echo $this->template->checked('3pkt') ?> name="3pkt" value="YES" id="3pkt"/></td>
                                                <td style="border-top: 0;">Hæftet <input type="checkbox" <?php echo $this->template->checked('haeftet') ?> name="haeftet" value="YES" id="haeftet"/></td>
                                                <td style="border-top: 0;">Tape <input type="checkbox" <?php echo $this->template->checked('tape') ?> name="tape" value="YES" id="tape"/></td>
                                                <td style="border-top: 0;">Ingen <input type="checkbox" <?php echo $this->template->checked('ingen') ?> name="ingen" value="YES" id="ingen"/></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div class="form-group col-lg-12">
                                        <label for="oplag">Oplag:</label>
                                        <input type="text" class="form-control" id="oplag" placeholder="Oplag" name="oplag" value="<?php echo set_value('oplag',$this->template->setFieldValue('oplag')) ?>">
                                        <?php echo form_error('oplag') ?>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="oplag">Targetpris:</label>
                                        <input type="text" class="form-control" id="Targetpris" placeholder="Targetpris" name="targetpris" value="<?php echo set_value('targetpris',$this->template->setFieldValue('targetpris')) ?>">
                                        <?php echo form_error('targetpris') ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="oplag">Salgspris:</label>
                                        <input type="text" class="form-control" id="Salgspris" placeholder="Salgspris" name="Salgspris" value="<?php echo set_value('Salgspris',$this->template->setFieldValue('Salgspris')) ?>">
                                        <?php echo form_error('Salgspris') ?>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
                <div class="col-lg-7">
                    <h3>&nbsp;</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td colspan="4">
                                    <label for="">Paller:</label>

                                    <table class="table">
                                        <tr>
                                            <td><label for="ordre">Engangspaller:</label>
                                                <input type="checkbox" <?php echo $this->template->checked('engangspaller') ?> id="engangspaller" placeholder="Engangspaller" name="engangspaller" value="YES"></td>
                                            <td>
                                                <label for="EUR">EUR:</label>
                                                <input type="checkbox" <?php echo $this->template->checked('EUR') ?> id="EUR" placeholder="EUR" name="EUR" value="YES">
                                            </td>
                                            <td>
                                                <p>Paller indregnes i pris og tages ikke retur</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <label for="">Bundtning:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Antal pr. bundt </span>
                                        <input type="text" style="width: 60px;" class="form-control" name="Antal_pr_bundt" id="Antal_pr_bundt" value="<?php echo set_value('Antal_pr_bundt',$this->template->setFieldValue('Antal_pr_bundt')) ?>"/>
                                        <span class="input-group-addon">stk.</span>


                                    </div>
                                    <?php echo form_error('Antal_pr_bundt') ?>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="Antal_pr_palle">Antal pr. palle:</label>
                                        <input type="text" name="Antal_pr_palle" class="form-control" id="Antal_pr_palle" placeholder="Antal pr. palle" value="<?php echo set_value('Antal_pr_palle',$this->template->setFieldValue('Antal_pr_palle')) ?>"/>
                                        <?php echo form_error('Antal_pr_palle') ?>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="3">
                                    <div class="form-group col-lg-12">
                                        <label>Max pallemål:</label>
                                        <input type="text" name="Max_pallemal" class="form-control" id="Max pallemal" placeholder="Max pallemål" value="<?php echo set_value('Max_pallemal',$this->template->setFieldValue('Max_pallemal')) ?>"/>
                                        <?php echo form_error('Max_pallemal') ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        Dobbelt stabling ikke tilladt
                                        <input type="checkbox" <?php echo $this->template->checked('Dobbelt_stabling') ?> name="Dobbelt_stabling" id="Dobbelt_stabling" value="YES"/>

                                    </div>
                                </td>
                            </tr>
                        </table>

                        <h3>Trykdetaljer:</h3>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <label>Trykform:</label>
                                    <table class="table">
                                        <tr>
                                            <td style="border-top: 0;">
                                                <label for="">Offset</label>
                                                <input type="checkbox" <?php echo $this->template->checked('offset') ?> name="offset" id="offset" placeholder="Offset" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Flexo</label>
                                                <input type="checkbox" <?php echo $this->template->checked('Flexo') ?> name="Flexo" id="Flexo" placeholder="Flexo" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Ingen</label>
                                                <input type="checkbox" <?php echo $this->template->checked('Ingen') ?> name="Ingen" id="Ingen" placeholder="Ingen" value="YES"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="">Tegnings nr.:</label>
                                        <input type="text" class="form-control" name="tegnings_nr" id="tegnings_nr" placeholder="Tegnings nr" value="<?php echo set_value('tegnings_nr',$this->template->setFieldValue('tegnings_nr')) ?>"/>
                                        <?php echo form_error('tegnings_nr') ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="">Antal farver:</label>
                                        <input type="text" class="form-control" name="Antal_farver" id="Antal_farver" placeholder="Antal farver" value="<?php echo set_value('Antal_farver',$this->template->setFieldValue('Antal_farver')) ?>"/>
                                        <?php echo form_error('Antal_farver') ?>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group col-lg-12">
                                        <label for="">Procent dækning v/flexo:</label>
                                        <input type="text" class="form-control" name="Procent_deakning" id="Procent_deakning" placeholder="Procent dækning v/flexo" value="<?php echo set_value('Procent_deakning',$this->template->setFieldValue('Procent_deakning')) ?>"/>
                                        <?php echo form_error('Procent_deakning') ?>
                                    </div>
                                </td>

                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div class="form-group col-lg-12">
                                        <label for="">Pantone nummer:</label>
                                        <input type="text" class="form-control" name="Pantone_nummer" id="Pantone_nummer" placeholder="Pantone nummer" value="<?php echo set_value('Pantone_nummer',$this->template->setFieldValue('Pantone_nummer')) ?>"/>
                                        <?php echo form_error('Pantone_nummer') ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <label for="">Lak</label>
                                    <table class="table">
                                        <tr>
                                            <td style="border-top: 0;">
                                                <label for="">Normal</label>
                                                <input type="checkbox" <?php echo $this->template->checked('normal') ?> name="normal" id="normal" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Blank</label>
                                                <input type="checkbox" <?php echo $this->template->checked('blank') ?> name="blank" id="blank" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Mat</label>
                                                <input type="checkbox" <?php echo $this->template->checked('Mat') ?> name="Mat" id="Mat" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Mat UV lak</label>
                                                <input type="checkbox" <?php echo $this->template->checked('mat_uv') ?> name="mat_uv" id="mat_uv" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Blank UV lak</label>
                                                <input type="checkbox" <?php echo $this->template->checked('Blank_UV') ?> name="Blank_UV" id="Blank_UV" value="YES"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <label for="">Korrektur tilsendes:</label>
                                    <table class="table">
                                        <tr>
                                            <td style="border-top: 0;">
                                                <label for="">Sælger</label>
                                                <input type="checkbox" <?php echo $this->template->checked('normal') ?> name="normal" id="normal" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Kunde</label>
                                                <input type="checkbox" <?php echo $this->template->checked('Kunde') ?> name="Kunde" id="Kunde" value="YES"/>
                                            </td>
                                            <td style="border-top: 0;">
                                                <label for="">Sælger</label>
                                                <input type="checkbox" <?php echo $this->template->checked('Salger') ?> name="Salger" id="Salger" value="YES"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <tr>
                                    <td>
                                        <div class="form-group col-lg-12">
                                            <label for="">Kundemail:</label>
                                            <input type="text" name="Kundemail" id="Kundemail" value="<?php echo set_value('Kundemail',$this->template->setFieldValue('Kundemail')) ?>" placeholder="Kundemail" class="form-control"/>
                                            <?php echo form_error('Kundemail') ?>
                                        </div>
                                    </td>
                                </tr>
                            </tr>
                        </table>
                    </div>
                </div>
            
            <div class="clear"></div>
            <div class="col-lg-12">
                <h3>Standardvarer / Bemærkninger:</h3>
                <textarea name="Standardvarer" id="Standardvarer" class="form-control" cols="30" rows="10"><?php echo set_value('Standardvarer', $this->template->setFieldValue('Standardvarer')) ?></textarea>
                <?php echo form_error('Standardvarer') ?>
            </div>
            </div>


            <hr/>
            <div class="mg_10">

                <div class="col-md-6">
                    <input type="submit"  name="submit" id="btn_submit" value="Submit" class="btn btn-success"/>

                </div>


            </div>

        </form>
    </div>


</div>

<?php if($this->uri->segment(2) == 'edit') : ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $("form :input").on('keyup', function(){
            $("#btn_submit").prop('disabled', false)
        });

        $("form input[type='checkbox'],form select").on('change', function(){
            $("#btn_submit").prop('disabled', false)
        })

        $("#btn_submit").prop('disabled', true)

    })
</script>
<?php endif; ?>