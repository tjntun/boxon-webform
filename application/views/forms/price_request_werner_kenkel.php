<div class="container">
<h1 class="text-center">Price Request form - Werner Kenkel</h1>

<hr/>

<form id="pricerequest" method="POST" action="<?php echo current_url(); ?>" class="form-horizontal text-left boxon-webforms">

<h3 class="text-left">
    Boxon Reference
</h3>

<div class="form-group">
    <label for="requestdate" class="col-md-3">
        Request Date:
    </label>

    <div class="col-md-3">

        <div class="input-group date datepicker no-padding" data-date-format="dd-mm-yyyy" id="requestdate">
            <input type='text' class="form-control datepicker-input" name="requestdate" value="<?php echo set_value('requestdate',$this->template->setFieldValue('requestdate')) ?>"/>
             <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
        </div>
        <?php echo form_error('requestdate') ?>
    </div>


    <label for="prreference" class="col-md-3">
        PR. Reference:
    </label>

    <div class="col-md-3">
        <input type="text" class="form-control" name="prreference" id="prreference" placeholder="Enter PR. Reference"
               value="<?php echo set_value('prreference',$this->template->setFieldValue('prreference')) ?>">
        <?php echo form_error('prreference'); ?>
    </div>
</div>


<div class="form-group">
    <label for="reference" class="col-md-3">
        Reference:
    </label>

    <div class="col-md-9">
        <input type="text" class="form-control" name="reference" id="reference" placeholder="Enter Reference"
               value="<?php echo set_value('reference',$this->template->setFieldValue('reference')) ?>">
        <?php echo form_error('reference'); ?>
    </div>
</div>


<div class="form-group">
    <label for="fefco" class="col-md-3">
        FEFCO:
    </label>

    <div class="col-md-3">
        <?php
        $fefco = array(
            '' => '--Please Select--','0200' => '0200','0201' => '0201','0202' => '0202','0203' => '0203','0204' => '0204','0205' => '0205','0206' => '0206','0207' => '0207','0208' => '0208','0209' => '0209','0211' => '0211','0212' => '0212','0214' => '0214','0215' => '0215','0216' => '0216','0217' => '0217','0218' => '0218','0225' => '0225','0226' => '0226','0300' => '0300','0301' => '0301','0302' => '0302','0303' => '0303','0304' => '0304','0305' => '0305','0306' => '0306','0307' => '0307','0310' => '0310','0311' => '0311','0312' => '0312','0313' => '0313','0320' => '0320','0321' => '0321','0322' => '0322','0323' => '0323','0325' => '0325','0330' => '0330','0401' => '0401','0402' => '0402','0403' => '0403','0404' => '0404','0405' => '0405','0406' => '0406','0407' => '0407','0409' => '0409','0410' => '0410','0411' => '0411','0412' => '0412','0413' => '0413','0415' => '0415','0416' => '0416','0420' => '0420','0421' => '0421','0422' => '0422','0423' => '0423','0424' => '0424','0425' => '0425','0426' => '0426','0427' => '0427','0428' => '0428','0429' => '0429','0430' => '0430','0431' => '0431','0432' => '0432','0433' => '0433','0434' => '0434','0435' => '0435','0436' => '0436','0437' => '0437','0440' => '0440','0441' => '0441','0442' => '0442','0443' => '0443','0444' => '0444','0445' => '0445','0446' => '0446','0447' => '0447','0450' => '0450','0451' => '0451','0452' => '0452','0453' => '0453','0454' => '0454','0455' => '0455','0456' => '0456','0457' => '0457','0458' => '0458','0459' => '0459','0460' => '0460','0470' => '0470','0501' => '0501','0502' => '0502','0503' => '0503','0504' => '0504','0505' => '0505','0507' => '0507','0508' => '0508','0509' => '0509','0601' => '0601','0602' => '0602','0605' => '0605','0606' => '0606','0607' => '0607','0608' => '0608','0610' => '0610','0615' => '0615','0616' => '0616','0620' => '0620','0621' => '0621','0700' => '0700','0701' => '0701','0703' => '0703','0711' => '0711','0712' => '0712','0713' => '0713','0714' => '0714','0715' => '0715','0716' => '0716','0717' => '0717','0718' => '0718','0747' => '0747','0748' => '0748','0751' => '0751','0752' => '0752','0759' => '0759','0760' => '0760','0761' => '0761','0770' => '0770','0771' => '0771','0772' => '0772','0773' => '0773','0774' => '0774','0900' => '0900','0901' => '0901','0902' => '0902','0903' => '0903','0904' => '0904','0905' => '0905','0906' => '0906','0907' => '0907','0908' => '0908','0909' => '0909','0910' => '0910','0911' => '0911','0913' => '0913','0914' => '0914','0920' => '0920','0921' => '0921','0929' => '0929','0930' => '0930','0931' => '0931','0932' => '0932','0933' => '0933','0934' => '0934','0935' => '0935','0940' => '0940','0941' => '0941','0942' => '0942','0943' => '0943','0944' => '0944','0945' => '0945','0946' => '0946','0947' => '0947','0948' => '0948','0949' => '0949','0950' => '0950','0951' => '0951','0965' => '0965','0966' => '0966','0967' => '0967','0970' => '0970','0971' => '0971','0972' => '0972','0973' => '0973','0974' => '0974','0975' => '0975','0976' => '0976',
        );
        echo form_dropdown('fefco', $fefco, set_value('fefco',$this->template->setFieldValue('fefco')), 'class="form-control"');
        echo form_error('fefco');
        ?>
    </div>


    <label for="quantity" class="col-md-3">
        Quantity:
    </label>

    <div class="col-md-3">
        <?php
        $quantity = array('' => '--Please Select--', 'B2B01' => ' B2B01', 'B2B02' => ' B2B02', 'B2B03' => ' B2B03', 'B2B04' => ' B2B04', 'B2B102' => ' B2B102', 'B2B103' => ' B2B103', 'B2B104' => ' B2B104', 'B2B106' => ' B2B106', 'B2B107' => ' B2B107', 'BB02' => ' BB02', 'BB03' => ' BB03', 'BB04' => ' BB04', 'BB05' => ' BB05', 'BB10' => ' BB10', 'BB11' => ' BB11', 'BB14' => ' BB14', 'BB09' => ' BB09', 'BB101' => ' BB101', 'BB104' => ' BB104', 'BB106' => ' BB106', 'BB107' => ' BB107', 'BB108' => ' BB108', 'BB109' => ' BB109', 'BB111' => ' BB111', 'BB113' => ' BB113', 'BB115' => ' BB115', 'BB116' => ' BB116', 'BB117' => ' BB117', 'BB118' => 'BB118', 'BB119' => 'BB119', 'BB120' => 'BB120', 'BB121' => 'BB121', 'BB122' => 'BB122', 'BB127' => 'BB127', 'BB128' => 'BB128', 'BB129' => 'BB129', 'BB131' => 'BB131', 'BB133' => 'BB133', 'BB134' => 'BB134', 'BB15' => 'BB15', 'BB135' => 'BB135', 'BB136' => 'BB136', 'BS02' => 'BS02', 'BS04' => 'BS04', 'BS10' => 'BS10', 'BS129' => 'BS129', 'BS13' => 'BS13', 'BS15' => 'BS15', 'BS16' => 'BS16', 'BS17' => 'BS17', 'BS06' => 'BS06', 'BS101' => 'BS101', 'BS102' => 'BS102', 'BS103' => 'BS103', 'BS104' => 'BS104', 'BS105' => 'BS105', 'BS106' => 'BS106', 'BS107' => 'BS107', 'BS108' => 'BS108', 'BS109' => 'BS109', 'BS110' => 'BS110', 'BS111' => 'BS111', 'BS112' => 'BS112', 'BS113' => 'BS113', 'BS114' => 'BS114', 'BS116' => 'BS116', 'BS117' => 'BS117', 'BS118' => 'BS118', 'BS120' => 'BS120', 'BS121' => 'BS121', 'BS122' => 'BS122', 'BS123' => 'BS123', 'BS125' => 'BS125', 'BS130' => 'BS130', 'BS131' => 'BS131', 'C2B01' => 'C2B01', 'C2B101' => 'C2B101', 'C2B102' => 'C2B102', 'C2B103' => 'C2B103', 'C2B104' => 'C2B104', 'C2B106' => 'C2B106', 'C2B107' => 'C2B107', 'C2B108' => 'C2B108', 'C2B109' => 'C2B109', 'C2B110' => 'C2B110', 'C2B111' => 'C2B111', 'C2B112' => 'C2B112', 'C2B113' => 'C2B113', 'C2B114' => 'C2B114', 'C2B115' => 'C2B115', 'C2B116' => 'C2B116', 'C2B118' => 'C2B118', 'C2B119' => 'C2B119', 'C2B120' => 'C2B120', 'C2B121' => 'C2B121', 'C2B122' => 'C2B122', 'C2B123' => 'C2B123', 'C2B125' => 'C2B125', 'E2B01' => 'E2B01', 'E2B02' => 'E2B02', 'E2B03' => 'E2B03', 'E2B101' => 'E2B101', 'E2B104' => 'E2B104', 'E2B105' => 'E2B105', 'E2B106' => 'E2B106', 'E2B107' => 'E2B107', 'E2B108' => 'E2B108', 'E2B109' => 'E2B109', 'E2B112' => 'E2B112', 'E2B114' => 'E2B114', 'EB01' => 'EB01', 'EB02' => 'EB02', 'EB\'11' => 'EB\'11', 'EB09' => 'EB09', 'EB101' => 'EB101', 'EB102' => 'EB102', 'EB103' => 'EB103', 'EB104' => 'EB104', 'EB105' => 'EB105', 'EB106' => 'EB106', 'EB107' => 'EB107', 'EB108' => 'EB108', 'EB111' => 'EB111', 'EB117' => 'EB117', 'EB12' => 'EB12', 'ES03' => 'ES03', 'ES10' => 'ES10', 'ES05' => 'ES05', 'ES101' => 'ES101', 'ES102' => 'ES102', 'ES103' => 'ES103', 'ES104' => 'ES104', 'ES105' => 'ES105', 'ES106' => 'ES106', 'ES107' => 'ES107', '4BE104' => '4BE104', '4EE103' => '4EE103', '4EE105' => '4EE105', 'BCB02' => 'BCB02', 'BCB03' => 'BCB03', 'BCB101' => 'BCB101', 'BCB102' => 'BCB102', 'BCB103' => 'BCB103', 'BCB105' => 'BCB105', 'BCB106' => 'BCB106', 'BCB107' => 'BCB107', 'BCB108' => 'BCB108', 'BCB109' => 'BCB109', 'BCB110' => 'BCB110', 'BCS02' => 'BCS02', 'BCS03' => 'BCS03', 'BCS10' => 'BCS10', 'BCS13' => 'BCS13', 'BCS101' => 'BCS101', 'BCS102' => 'BCS102', 'BCS103' => 'BCS103', 'BCS104' => 'BCS104', 'BCS106' => 'BCS106', 'BCS107' => 'BCS107', 'BCS108' => 'BCS108', 'BCS11' => 'BCS11', 'BCS110' => 'BCS110', 'BCS111' => 'BCS111', 'BCS112' => 'BCS112', 'BCS113' => 'BCS113', 'BCS115' => 'BCS115', 'BCS116' => 'BCS116', 'BCS117' => 'BCS117', 'BCS118' => 'BCS118', 'BCS119' => 'BCS119', 'BCS120' => 'BCS120', 'BCS122' => 'BCS122', 'BCS123' => 'BCS123', 'BCS124' => 'BCS124', 'BCS125' => 'BCS125', 'BCS14' => 'BCS14', 'BCS126' => 'BCS126', 'BE2B02' => 'BE2B02', 'BE2B101' => 'BE2B101', 'BE2B103' => 'BE2B103', 'BE2B104' => 'BE2B104', 'BE2B105' => 'BE2B105', 'BE2B107' => 'BE2B107', 'BE2B108' => 'BE2B108', 'BEB01' => 'BEB01', 'BEB02' => 'BEB02', 'BEB07' => 'BEB07', 'BEB08' => 'BEB08', 'BEB09' => 'BEB09', 'BEB101' => 'BEB101', 'BEB102' => 'BEB102', 'BEB103' => 'BEB103', 'BEB104' => 'BEB104', 'BEB105' => 'BEB105', 'BEB108' => 'BEB108', 'BEB110' => 'BEB110', 'BEB112' => 'BEB112', 'BEB113' => 'BEB113', 'BEB114' => 'BEB114', 'BEB115' => 'BEB115', 'BEB116' => 'BEB116', 'BEB117' => 'BEB117', 'CB01' => 'CB01', 'CB05' => 'CB05', 'CB101' => 'CB101', 'CB102' => 'CB102', 'CB103' => 'CB103', 'CB104' => 'CB104', 'CB105' => 'CB105', 'CB106' => 'CB106', 'CB107' => 'CB107', 'CB108' => 'CB108', 'CB109' => 'CB109', 'CB110' => 'CB110', 'CB111' => 'CB111', 'CB126' => 'CB126', 'CB13' => 'CB13', 'CB16' => 'CB16', 'CB17' => 'CB17', 'CB19' => 'CB19', 'CB20' => 'CB20', 'CS01' => 'CS01', 'CS04' => 'CS04', 'CS101' => 'CS101');
        echo form_dropdown('quantity', $quantity, set_value('quantity',$this->template->setFieldValue('quantity')), 'class="form-control"');
        echo form_error('quantity');
        ?>
    </div>


</div>

<div class="form-group">
    <label for="insidedimensions" class="col-md-3">
        Inside Dimensions:
    </label>

    <div class="col-md-2">
        <input type="text" class="form-control" name="length" id="length" placeholder="Length"
               value="<?php echo set_value('length',$this->template->setFieldValue('length')) ?>">
        <?php echo form_error('length'); ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="width" id="width" value="<?php echo set_value('width',$this->template->setFieldValue('width')) ?>"
               placeholder="Width">
        <?php echo form_error('width'); ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" value="<?php echo set_value('height',$this->template->setFieldValue('height')) ?>" name="height" id="height"
               placeholder="Height">
        <?php echo form_error('height'); ?>
    </div>

</div>


<div class="form-group">
    <label for="print" class="col-md-3">
        Print:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('print', array('' => '--Please Select--', 'No' => 'No', '1 Color' => '1 Color', '2 Color' => '2 Color', '3 Color' => '3 Color', '4 Color' => '4 Color', '5 Color' => '5 Color'), set_value('print',$this->template->setFieldValue('print')), 'class="form-control"') ?>
        <?php echo form_error('print') ?>
    </div>


    <label for="coverage" class="col-md-3">
        Coverage:
    </label>

    <div class="col-md-3">
        <?php
        $coverage = array('0' => '--Please Select--', 5 => '5', 10 => '10', 15 => '15', 20 => '20', 25 => '25', 30 => '30', 35 => '35', 40 => '40', 45 => '45', 50 => '50', 55 => '55', 60 => '60', 65 => '65', 70 => '70', 75 => '75', 80 => '80', 85 => '85', 90 => '90', 95 => '95', 100 => '100');
        echo form_dropdown('coverage', $coverage, set_value('coverage',$this->template->setFieldValue('coverage')), 'class="form-control"');
        echo form_error('coverage');
        ?>
    </div>


</div>

<div class="form-group">
    <label for="assembling" class="col-md-3">
        Assembling:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('assembling', array('' => '--Please Select--', 'None' => 'None', 'Glued' => 'Glued', 'Stiched' => 'Stiched'), set_value('assembling',$this->template->setFieldValue('assembling')), 'class="form-control"') ?>
        <?php echo form_error('assembling') ?>
    </div>
</div>


<div class="form-group">
    <label for="quantity" class="col-md-3">
        Quantity:
    </label>

    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity1" value="<?php echo set_value('quantity1',$this->template->setFieldValue('quantity1')) ?>"
               id="quantity1" placeholder="Quantity">
        <?php echo form_error('quantity1') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity2" value="<?php echo set_value('quantity2',$this->template->setFieldValue('quantity2')) ?>"
               id="quantity2" placeholder="Quantity">
        <?php echo form_error('quantity2') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity3" value="<?php echo set_value('quantity3',$this->template->setFieldValue('quantity3')) ?>" id="quantity3"
               placeholder="Quantity">
        <?php echo form_error('quantity3') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="quantity4" value="<?php echo set_value('quantity4',$this->template->setFieldValue('quantity4')) ?>" id="quantity4"
               placeholder="Quantity">
        <?php echo form_error('quantity4') ?>
    </div>
</div>

<div class="form-group">
    <label for="bundled" class="col-md-3">
        Bundled:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('bundled', array('' => '--Please Select--', 'Yes' => 'Yes', 'No' => 'No'), set_value('bundled',$this->template->setFieldValue('bundled')), 'class="form-control"') ?>
        <?php echo form_error('bundled') ?>
    </div>

    <label for="peicesbundle" class="col-md-3">
        Peices Per Bundle:
    </label>

    <div class="col-md-3">
        <input type="text" class="form-control" name="peicesbundle" value="<?php echo set_value('peicesbundle',$this->template->setFieldValue('peicesbundle')) ?>"
               id="peicesbundle" placeholder="Enter Bundle Pieces">
        <?php echo form_error('peicesbundle') ?>
    </div>
</div>


<div class="form-group">
    <label for="strecthfilmonpallets" class="col-md-3">
        Strecth Film On Pallets:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('strecthfilmonpallets', array('' => '--Please Select--', 'Yes' => 'Yes', 'No' => 'No'), set_value('strecthfilmonpallets',$this->template->setFieldValue('strecthfilmonpallets')), 'class="form-control"') ?>
        <?php echo form_error('strecthfilmonpallets') ?>
    </div>
</div>

<div class="form-group">
    <label for="quantityperpallet" class="col-md-3">
        Quantity Per Pallet:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('quantityperpallet', array('' => '--Please Select--', 'Fixed' => 'Fixed', 'Optimized' => 'Optimized'), set_value('quantityperpallet',$this->template->setFieldValue('quantityperpallet')), 'class="form-control"') ?>
        <?php echo form_error('quantityperpallet') ?>
    </div>

    <div class="col-md-3">
        <input type="text" class="form-control" name="peicesbundle" value="<?php echo set_value('peicesbundle',$this->template->setFieldValue('peicesbundle')) ?>" id="peicesbundle" placeholder="Quantity Per Pallet">
        <?php echo form_error('peicesbundle') ?>
    </div>

</div>

<div class="form-group">
    <label for="maxpalletdimension" class="col-md-3">
        Max Pallet Dimension:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('maxpalletdimension', array('' => '--Please Select--', 'Yes' => 'Yes', 'No' => 'No'), set_value('maxpalletdimension',$this->template->setFieldValue('maxpalletdimension')), 'class="form-control"') ?>
        <?php echo form_error('maxpalletdimension') ?>
    </div>
</div>

<div class="form-group">
    <label for="dimension" class="col-md-3">
        Dimension:
    </label>

    <div class="col-md-2">
        <input type="text" class="form-control" name="length1" value="<?php echo set_value('length1',$this->template->setFieldValue('length1')) ?>" id="length1" placeholder="Length">
        <?php echo form_error('length1') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="width1" id="width1"
               placeholder="Width" value="<?php echo set_value('width1',$this->template->setFieldValue('width1')) ?>">
        <?php echo form_error('width1') ?>
    </div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="height1" id="height1" placeholder="Height" value="<?php echo set_value('height1',$this->template->setFieldValue('height1')) ?>">
        <?php echo form_error('height1') ?>
    </div>
</div>


<div class="form-group">
    <label for="pallet" class="col-md-3">
        Pallet:
    </label>

    <div class="col-md-3">
        <?php echo form_dropdown('pallet', array('' => '--Please Select--', 'One Way' => 'One Way', 'EUR' => 'EUR', 'New EUR' => 'New EUR'), set_value('pallet',$this->template->setFieldValue('pallet')), 'class="form-control"') ?>
        <?php echo form_error('pallet') ?>
    </div>
</div>

<div class="row fixed-form-footer">
    <div class="center">
        <input type="submit" style="margin-right: 15px;" value="Save" id="btn_submit" class="btn btn-success" name="submit"/>
        <button type="reset" style="margin-right: 15px;" class="btn btn-default" name="reset">Reset</button>
        <?php if (isset($action) && $action == 'edit'): ?>
        <a href="#" class="btn btn-default" style="margin-right: 15px;" id="print-btn">Print</a>
        <a href="<?php echo site_url('/price_request_werner_kenkel/email/'.$form['ID']) ?>" class="btn btn-default" style="margin-right: 15px;" >Email me</a>
        <?php endif; ?>
        <a href="<?php echo site_url('/price_request_werner_kenkel') ?>" class="btn btn-default">Back</a>
    </div>
</div>
</form>
</div>
<?php if($this->uri->segment(2) == 'edit') : ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#print-btn').click(function(event) {
                $('.fixed-form-footer, #myinfo-btn, #logout-btn').hide();
                window.print();
                $('.fixed-form-footer, #myinfo-btn, #logout-btn').show();
            });

            $("form :input").on('keyup', function(){
                $("#btn_submit").prop('disabled', false)
            });

            $("form input[type='checkbox'],form select").on('change', function(){
                $("#btn_submit").prop('disabled', false)
            })

            $("#btn_submit").prop('disabled', true)
        })
    </script>
<?php endif; ?>