<div class="container">
    <div class="wrap">

        <div class="row">
            <!--   <h2 class="text-left">
                  Price request form - Werner Kenkel
              </h2> -->
            <h2 class="text-center">
                JIT Calculation - Tab 2
            </h2>
        </div>


        <!-- <form class="form-horizontal text-left" role="form"
             name="contactform" method="post" action="send_form_email.php"> -->

        <form id="pricerequest" method="POST" action="html2pdf3.php" class="form-horizontal text-left" role="form">


            <div class="form-group">
                <label for="reference" class="col-md-3">
                    Leveringstid, uger:
                </label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="reference" id="reference" placeholder="Enter Reference" required="">
                </div>
            </div>


            <div class="form-group">
                <label for="postnummer" class="col-md-3">
                    Kundes postnummer:
                </label>
                <div class="col-md-3">
                    <select name="postnummer" id="postnummer" class="form-control">
                        <option>--Please Select--</option>
                        <option> 0201 </option>
                        <option> 0202 </option>
                        <option> 0203 </option>
                        <option> 0204 </option>
                        <option> 0205 </option>
                        <option> 0206 </option>
                        <option> 0207 </option>
                        <option> 0208 </option>
                        <option> 0209 </option>
                        <option> 0211 </option>
                        <option> 0212 </option>
                        <option> 0214 </option>
                        <option> 0215 </option>
                        <option> 0216 </option>
                        <option> 0217 </option>
                        <option> 0218 </option>
                        <option> 0225 </option>
                        <option> 0226 </option>
                        <option> 0230 </option>
                        <option> 0209 </option>
                        <option> 0211 </option>
                        <option> 0212 </option>
                    </select>
                </div>



            </div>




            <br>


            <div class="bs-example">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Reference</th>
                            <th>Oplag/Lev</th>
                            <th>Antal/palle</th>
                            <th>Paller fullload</th>
                            <th>EUR 1000 styk</th>
                            <th>Stykpris inkl. fragt</th>
                            <th>Vælg DG2</th>
                            <th>Salgspris pr. styk</th>
                            <th>Vælg salgspris</th>
                            <th>DG2 </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Shah</td>
                            <td>Yunus</td>
                            <td>softkids@mail.com</td>

                            <td>1</td>
                            <td>Shah</td>
                            <td>Yunus</td>
                            <td>softkids@mail.com</td>

                            <td>1</td>
                            <td>Shah</td>




                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Shah</td>
                            <td>Yunus</td>
                            <td>softkids@mail.com</td>

                            <td>1</td>
                            <td>Shah</td>
                            <td>Yunus</td>
                            <td>softkids@mail.com</td>

                            <td>1</td>
                            <td>Shah</td>

                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Shah</td>
                            <td>Yunus</td>
                            <td>softkids@mail.com</td>

                            <td>1</td>
                            <td>Shah</td>
                            <td>Yunus</td>
                            <td>softkids@mail.com</td>

                            <td>1</td>
                            <td>Shah</td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>



            <div class="row">

                <div class="col-md-3">
                </div>

                <div class="col-md-6">
                    <button type="submit" class="btn btn-success" name="submit">
                        Generate PDF
                    </button>
                    <button type="reset" class="btn btn-danger" name="reset">
                        Reset
                    </button>
                </div>


            </div>
        </form>

        <br>
        <br>

    </div>
</div>