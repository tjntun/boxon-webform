<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FEFCO 0201 beregning(calculation)</title>


    <link rel="stylesheet" href="<?php echo site_url('/assets/css/grid.css') ?>"/>

</head>
<body>
<div class="container">
    <h1 class="text-center">Prisberegning FEFCO 0201</h1>

    <hr/>


    <table width="98%" class="table table-bordered">
        <tr>
            <td width="25%">Reference: </td>
            <td><b><?php echo $this->template->formData['reference'] ?></b></td>
            <td>Kvalitet: </td>
            <td><b><?php echo $this->template->formData['quality'] ?></b></td>
        </tr>
        <tr>
            <td>Indvendige mal:</td>
            <td> Length : <b><?php echo $this->template->formData['length']; ?></b></td>
            <td> Width : <b><?php echo $this->template->formData['width']; ?></b> </td>
            <td>Height : <b><?php echo $this->template->formData['height']; ?></b></td>

        </tr>

        <tr>
            <td>Oplag: </td>
            <td><b><?php echo $this->template->formData['warehouse']; ?></b></td>
            <td>Total m2: </td>
            <td><b><?php echo $this->template->formData['totalm2']; ?></b></td>
        </tr>


        <tr>
            <td colspan="2"></td>
            <td>Foresporgsel Accepteret:</td>
            <td><b><?php echo $this->template->formData['requestaccepted']; ?></b></td>
        </tr>

        <tr>
            <td></td>
            <td style="text-align: right;"><span style="color: red;">OBS</span></td>
            <td colspan="2">

                <span style="color: red;">Lorem Ipsum is simply dummy text of the <br/> printing and typesetting industry.</span>

            </td>
        </tr>



        <tr>
            <td colspan="4" style="text-align: center;"> Ordre iDKK </td>
        </tr>

        <tr>
            <td colspan="2" style="text-align: center;"> Kostpris (Cost) </td>
            <td colspan="2" style="text-align: center;"> Salgspris (Sales Price) </td>
        </tr>

        <tr>
            <td>Per Stock :<b><?php echo $this->template->formData['costqty']; ?></b></td>
            <td>Total : <b><?php echo $this->template->formData['costtotal']; ?></b></td>
            <td>Per Stock : <b><?php echo $this->template->formData['spqty']; ?></b></td>
            <td>Total : <b><?php echo $this->template->formData['sptotal']; ?></b></td>
        </tr>


    </table>


</div>

</body>
</html>
