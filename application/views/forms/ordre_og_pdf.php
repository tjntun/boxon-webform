<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>


    <link rel="stylesheet" href="<?php echo site_url('/assets/css/grid.css') ?>"/>

</head>
<body>
<div class="container">
<div class="row">
    <h2 class="text-center" style="text-align: center;">
        ORDRE- OG FORESPØRGSELSFORMULAR
    </h2>
</div>

<form action="<?php echo current_url(); ?>" class="form-horizontal" method="post">
<h3>Kundedetaljer</h3>

<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <td colspan="2">
                <div class="form-group col-lg-12">
                    <label for="firmanavn">Firmanavn:</label>
                    <?php echo $this->template->setFieldValue('firmanavn') ?>
                    <?php echo form_error('firmanavn') ?>
                </div>
            </td>
            <td>
                <div class="form-group col-lg-12">
                    <label for="CVR_nr">CVR nr:</label>
                    <?php echo $this->template->setFieldValue('CVR_nr') ?>

                </div>
            </td>
            <td>
                <div class="form-group col-lg-12">
                    <label for="saelger">Sælger:</label>
                    <?php echo $this->template->setFieldValue('saelger') ?>

                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group col-lg-12">
                    <label for="saelger">Kontaktperson:</label>
                    <?php echo $this->template->setFieldValue('kontaktperson') ?>

                </div>
            </td>
            <td>
                <div class="form-group col-lg-12">
                    <label for="saelger">Mail:</label>
                    <?php echo $this->template->setFieldValue('mail') ?>

                </div>
            </td>
            <td>
                <div class="form-group col-lg-12">
                    <label for="saelger">Telefonnummer:</label>
                    <?php echo $this->template->setFieldValue('telefonnummer') ?>

                </div>
            </td>
            <td>
                <div class="form-group col-lg-12">
                    <label for="saelger">Fakturamail:</label>
                    <?php echo $this->template->setFieldValue('fakturamail') ?>

                </div>

            </td>

        </tr>

        <tr>
            <td colspan="2">
                <div class="form-group col-lg-12">
                    <label for="faktureringsadresse">Faktureringsadresse:</label>
                    <?php echo $this->template->setFieldValue('faktureringsadresse') ?>

                </div>
            </td>
            <td colspan="2">
                <div class="form-group col-lg-12">
                    <label for="leveringsadresse">Leveringsadresse:</label>
                    <?php echo $this->template->setFieldValue('leveringsadresse') ?>

                </div>
            </td>
        </tr>
    </table>
</div>
<div class="row">
<div class="col-lg-5">
    <div class="table-responsive">
        <h3>Opgavedetaljer</h3>


        <table class="table table-bordered">
            <tr>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="dato">Dato:</label>
                        <?php echo $this->template->setFieldValue('dato') ?>

                    </div>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="leveringsdato">Leveringsdato:</label>
                        <?php echo $this->template->setFieldValue('leveringsdato') ?>

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="forespørgsel">Forespørgsel:</label>
                        <?php echo $this->template->setFieldValue('foresporgsel') ?>


                    </div>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="ordre">Ordre:</label>
                        <?php echo $this->template->setFieldValue('ordre') ?>

                    </div>
                </td>
            </tr>

        </table>

        <table class="table table-bordered">
            <tr>
                <td colspan="2">
                    <div class="form-group col-lg-12">
                        <label for="id_ref">ID/ref.:</label>
                        <?php echo $this->template->setFieldValue('id_ref') ?>

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="fefco">Fefco:</label>
                        <?php echo $this->template->setFieldValue('fefco') ?>

                    </div>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="Prove_vedlagt">Prøve vedlagt:</label>
                        <?php echo $this->template->setFieldValue('Prove_vedlagt') ?>

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="kvalitet">Kvalitet:</label>
                        <?php echo $this->template->setFieldValue('kvalitet') ?>

                    </div>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="flute">Flute:</label>
                        <?php echo $this->template->setFieldValue('flute') ?>

                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="form-group col-lg-12">
                        <label for="mal">Mal:</label>
                        <?php echo $this->template->setFieldValue('mal') ?>

                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="form-group col-lg-12">
                        <label for="mal">Hjørnelukning:</label>

                        <table class="table">
                            <tr>
                                <td style="border-top: 0;">Lim <?php echo $this->template->setFieldValue('lim') ?></td>
                                <td style="border-top: 0;">3
                                    pkt. <?php echo $this->template->setFieldValue('3pkt') ?> </td>
                                <td style="border-top: 0;">
                                    Hæftet <?php echo $this->template->setFieldValue('haeftet') ?></td>
                                <td style="border-top: 0;">
                                    Tape <?php echo $this->template->setFieldValue('tape') ?></td>
                                <td style="border-top: 0;">
                                    Ingen <?php echo $this->template->setFieldValue('ingen') ?></td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <div class="form-group col-lg-12">
                        <label for="oplag">Oplag:</label>
                        <?php echo $this->template->setFieldValue('oplag') ?>

                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="oplag">Targetpris:</label>
                        <?php echo $this->template->setFieldValue('Targetpris') ?>

                    </div>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="oplag">Salgspris:</label>
                        <?php echo $this->template->setFieldValue('Salgspris') ?>

                    </div>
                </td>
            </tr>
        </table>

    </div>
</div>
<div class="col-lg-7">
    <h3>&nbsp;</h3>

    <div class="table-responsive">
        <table class="table table-bordered">
            <tr>
                <td colspan="4">
                    <label for="">Paller:</label>

                    <table class="table" style="margin-bottom: 0;">
                        <tr>
                            <td style="border-top: 0; padding-bottom: 0;"><label for="ordre">Engangspaller:</label>
                                <?php echo $this->template->setFieldValue('engangspaller') ?>
                            </td>
                            <td style="border-top: 0;">
                                <label for="EUR">EUR:</label>
                                <?php echo $this->template->setFieldValue('EUR') ?>
                            </td>
                            <td style="border-top: 0;">
                                <p>Paller indregnes i pris og tages ikke retur</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <label for="">Bundtning:</label>

                    <div class="input-group">
                        <span class="input-group-addon">Antal pr. bundt </span>
                        <?php echo $this->template->setFieldValue('Antal_pr_bundt') ?>
                        <span class="input-group-addon">stk.</span>
                    </div>
                    <?php echo form_error('Antal_pr_bundt') ?>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="Antal_pr_palle">Antal pr. palle:</label>
                        <?php echo $this->template->setFieldValue('Antal_pr_palle') ?>

                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="3">
                    <div class="form-group col-lg-12">
                        <label>Max pallemål:</label>
                        <?php echo $this->template->setFieldValue('Max_pallemal') ?>

                    </div>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        Dobbelt stabling ikke tilladt
                        <?php echo $this->template->setFieldValue('Dobbelt_stabling') ?>


                    </div>
                </td>
            </tr>
        </table>

        <h3>Trykdetaljer:</h3>
        <table class="table table-bordered">
            <tr>
                <td>
                    <label>Trykform:</label>
                    <table class="table">
                        <tr>
                            <td style="border-top: 0;">
                                <label for="">Offset</label>
                                <?php echo $this->template->setFieldValue('offset') ?>

                            </td>
                            <td style="border-top: 0;">
                                <label for="">Flexo</label>
                                <?php echo $this->template->setFieldValue('Flexo') ?>

                            </td>
                            <td style="border-top: 0;">
                                <label for="">Ingen</label>
                                <?php echo $this->template->setFieldValue('Ingen') ?>

                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="">Tegnings nr.:</label>
                        <?php echo $this->template->setFieldValue('tegnings_nr') ?>

                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="">Antal farver:</label>
                        <?php echo $this->template->setFieldValue('Antal_farver') ?>

                    </div>
                </td>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="">Procent dækning v/flexo:</label>
                        <?php echo $this->template->setFieldValue('Procent_deakning') ?>

                    </div>
                </td>

            </tr>

            <tr>
                <td colspan="2">
                    <div class="form-group col-lg-12">
                        <label for="">Pantone nummer:</label>
                        <?php echo $this->template->setFieldValue('Pantone_nummer') ?>

                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label for="">Lak</label>
                    <table class="table">
                        <tr>
                            <td style="border-top: 0;">
                                <label for="">Normal</label>
                                <?php echo $this->template->setFieldValue('normal') ?>

                            </td>
                            <td style="border-top: 0;">
                                <label for="">Blank</label>
                                <?php echo $this->template->setFieldValue('blank') ?>

                            </td>
                            <td style="border-top: 0;">
                                <label for="">Mat</label>
                                <?php echo $this->template->setFieldValue('Mat') ?>

                            </td>
                            <td style="border-top: 0;">
                                <label for="">Mat UV lak</label>
                                <?php echo $this->template->setFieldValue('mat_uv') ?>

                            </td>
                            <td style="border-top: 0;">
                                <label for="">Blank UV lak</label>
                                <?php echo $this->template->setFieldValue('Blank_UV') ?>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table class="table table-bordered" style="margin-top: 50px;">
            <tr>
                <td>
                    <label for="">Korrektur tilsendes:</label>
                    <table class="table">
                        <tr>
                            <td style="border-top: 0;">
                                <label for="">Sælger</label>

                                <?php echo $this->template->setFieldValue('normal') ?>
                            </td>
                            <td style="border-top: 0;">
                                <label for="">Kunde</label>

                                <?php echo $this->template->setFieldValue('Kunde') ?>
                            </td>
                            <td style="border-top: 0;">
                                <label for="">Sælger</label>

                                <?php echo $this->template->setFieldValue('Salger') ?>
                            </td>
                        </tr>
                    </table>
                </td>
            <tr>
                <td>
                    <div class="form-group col-lg-12">
                        <label for="">Kundemail:</label>

                        <?php echo $this->template->setFieldValue('Kundemail') ?>

                    </div>
                </td>
            </tr>

        </table>
    </div>
</div>

<div class="clear"></div>
<div class="col-lg-12">
    <h3>Standardvarer / Bemærkninger:</h3>

    <table class="table table-bordered" width="99%">
        <tr>
            <td width="100%"><?php echo $this->template->setFieldValue('Standardvarer') ?></td>
        </tr>
    </table>

</div>
</div>


</div>

<style>
    .form-group { margin-bottom: 0; }
    td .table { margin-bottom: 0; }
</style>


</body>
</html>
