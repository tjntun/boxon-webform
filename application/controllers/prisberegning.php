<?php
/**
 * Created by Touqeer Shafi.
 * File : prisberegning.php
 * Date: 8/7/14
 * Time: 11:22 AM
 */
class prisberegning extends CI_Controller
{

    public function __construct(){
        parent::__construct();

        if(!$this->user->is_logged_in()) {
            $this->template->set_alert('danger','You must be logged in to use that page');
            redirect('/home');
        }
    }


    public function add() {
        $this->load->library('form_validation');

        if($this->input->post('submit') != FALSE) {

            $this->form_validation->set_rules('reference','reference','required|trim');
            $this->form_validation->set_rules('quality','quality','required|trim');
            $this->form_validation->set_rules('length','length','required|trim');
            $this->form_validation->set_rules('width','width','required|trim');
            $this->form_validation->set_rules('height','height','required|trim');
            $this->form_validation->set_rules('warehouse','warehouse','required|trim');
            $this->form_validation->set_rules('totalm2','Total m2','required|trim');
            $this->form_validation->set_rules('requestaccepted','Foresporgsel Accepteret','required|trim');
            $this->form_validation->set_rules('costqty','Quantity','required|trim');
            $this->form_validation->set_rules('costtotal','Total','required|trim');
            $this->form_validation->set_rules('spqty','spqty','required|trim');
            $this->form_validation->set_rules('sptotal','sptotal One','required|trim');
            $this->form_validation->set_rules('percentage','percentage','required|trim');
            $this->form_validation->set_rules('profitqty','profitqty','required|trim');
            $this->form_validation->set_rules('profittotal','profittotal','required|trim');


            $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

            if($this->form_validation->run() == true) {

                unset($_POST['submit']);

                $data['UserID'] = $this->user->user['ID'];
                $data['Created'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);
                $data['FormType'] = 'prisberegning';

                $this->db->insert('user_form',$data);

                $this->template->set_alert('success','<strong>Success :</strong> Perice Wener Form Added Successfully');
                redirect(site_url('/forms'));

            } else {
                echo '<pre>' . print_r($_POST, 1) . '</pre>';
                die();
            }

            



        }

        $this->template->view('forms/prisberegning');
    }


    public function edit($formID = ''){
        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $this->load->library('form_validation');

        if($this->input->post('submit') != FALSE) {

            $this->form_validation->set_rules('reference','reference','required|trim');
            $this->form_validation->set_rules('quality','quality','required|trim');
            $this->form_validation->set_rules('length','length','required|trim');
            $this->form_validation->set_rules('width','width','required|trim');
            $this->form_validation->set_rules('height','height','required|trim');
            $this->form_validation->set_rules('warehouse','warehouse','required|trim');
            $this->form_validation->set_rules('totalm2','Total m2','required|trim');
            $this->form_validation->set_rules('requestaccepted','Foresporgsel Accepteret','required|trim');
            $this->form_validation->set_rules('costqty','Quantity','required|trim');
            $this->form_validation->set_rules('costtotal','Total','required|trim');
            $this->form_validation->set_rules('spqty','spqty','required|trim');
            $this->form_validation->set_rules('sptotal','sptotal One','required|trim');
            $this->form_validation->set_rules('percentage','percentage','required|trim');
            $this->form_validation->set_rules('profitqty','profitqty','required|trim');
            $this->form_validation->set_rules('profittotal','profittotal','required|trim');


            $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

            if($this->form_validation->run() == true) {

                unset($_POST['submit']);

                $data['Modified'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);

                $this->db->where('ID', $formID);
                $this->db->update('user_form',$data);

                $this->template->set_alert('success','<strong>Success :</strong> Perice Wener Form Edited Successfully');
                redirect(site_url('/forms'));

            }
        }


        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'],true);

        $this->template->view('forms/prisberegning',array('form' => $form));

    }



    public function generatePdf($formID = ''){

        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'],true);

        $this->load->helper(array('dompdf', 'file'));
        $html = $this->load->view('forms/prisberegning_pdf', null, true);
        pdf_create($html, 'Prisberegning');

    }

    public function email($formID = ''){

        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'],true);

        $this->load->library('form_validation');


        if($this->input->post('submit') != FALSE) {
            $this->form_validation->set_rules('emails','Emails','required|valid_emails');
            $this->form_validation->set_rules('subject','Subject','required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            if($this->form_validation->run() == true) {
                $this->load->library('email');

                $emails = explode(',',$this->input->post('emails'));
                $this->load->helper(array('dompdf', 'file'));

                $html = $this->load->view('forms/prisberegning_pdf', null, true);
                $pdf = pdf_create($html, 'Price_werner',false);
                $fileName = 'pdf-'.time('now').'.pdf';
                write_file('./uploads/'.$fileName,$pdf);


                foreach($emails as $email) {
                    $this->email->clear();
                    $this->email->from($this->config->item('EMAIL_FROM'), 'PDF Generator');
                    $this->email->to($email);

                    $this->email->subject($this->input->post('subject'));
                    $this->email->message($this->input->post('message'));


                    $this->email->attach('./uploads/'.$fileName);
                    $this->email->send();
                }

                $this->template->set_alert('success','<strong>Success : </strong> Email sent successfully');
                unlink(APPPATH.'/uploads/'.$fileName);
                redirect(site_url('/forms'));

            }

        }

        $this->template->view('email');
    }

}


/**
 * End File : prisberegning.php
 */ 