<?php
/*
 * Created By : Touqeer Shafi
 * Email : touqeer.shafi@gmail.com
 * Date : 8/1/14
 * File : auth.php 
 */
 
class auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){

        $this->load->library('form_validation');

        if ($this->input->post('submit') != FALSE) {

            $this->form_validation->set_rules('user_email', 'User Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('user_pass', 'User Password', 'trim|required');

            if ($this->form_validation->run() == true) {

                if ($this->user->auth($this->input->post('user_email'), $this->input->post('user_pass'),'admin')) {
                    $this->template->set_alert('success', 'You are now logged in');
                    redirect('/admin/dashboard');
                } else {
                    $this->template->set_alert('info', 'Username of password incorrect');
                }

            } else {
                $this->template->set_alert('danger', 'Validation Error');
            }
        }

        $this->template->view('admin/auth');

    }


    
}

 
/*
 * End File auth.php 
 */

 