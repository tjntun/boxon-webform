<?php

/*
 * Created By : Touqeer Shafi
 * Email : touqeer.shafi@gmail.com
 * Date : 8/1/14
 * File : dashboard.php 
 */

class dashboard extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        if (!$this->user->is_logged_in('admin')) {
            redirect('/admin/auth');
        }
    }
    
    public function logout()
    {
        if ($this->user->is_logged_in('admin')) {
            $this->user->logout('admin');
            redirect('/admin/auth');
        }
    }

    public function login_as_user($user)
    {
        $this->user->logout('user');
        redirect('/');
    }

    public function index()
    {

        $this->load->library('pagination');

        $this->db->start_cache();
        $this->db->select('*')->from('user')->where('group', 'user')->order_by('ID','DESC');
        $this->db->stop_cache();

        $per_page = 10;

        $total_rows = $this->db->count_all_results();


        $config['base_url'] = site_url('/admin/dashboard/index/page/');
        $config['total_rows'] = intval($total_rows);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '</li>';
        $config['first_tag_close'] = '<li>';

        $current_page = $this->uri->segment(5) == false ? 0 : intval($this->uri->segment(5));


        $this->pagination->initialize($config);
        $data['users'] = $this->db->get(NULL, $per_page, $current_page)->result_array();

        $this->template->view('admin/users', $data);
    }


    public function delete($ids = ''){
        $ids = explode('-', $ids);

        if(count($ids) > 0 ) {
            $this->db->where_in('ID', $ids)->delete('user');

            $this->template->set_alert('success','User(s) Deleted Successfully');
        } else {
            $this->template->set_alert('danger','Some thing went wrong');
        }


        if($this->input->server('HTTP_REFERRER') != FALSE)
            redirect($this->input->server('HTTP_REFERRER'));
        else
            redirect(site_url('/admin/dashboard'));

    }

    public function change_status($status, $ids){
        $ids = explode('-', $ids);

        if(count($ids) > 0){
            $this->db->where_in('ID',$ids)->update('user',array('Status' => $status));
            $this->template->set_alert('success','Status Changed successfully');
        } else {
            $this->template->set_alert('danger','Some thing went wrong');
        }


        if($this->input->server('HTTP_REFERRER') != FALSE)
            redirect($this->input->server('HTTP_REFERRER'));
        else
            redirect(site_url('/admin/dashboard'));

    }


    public function add(){

        $this->load->library('form_validation');


        if($this->input->post('submit') != FALSE){
            $this->form_validation->set_rules('FullName','Full Name','required');
            $this->form_validation->set_rules('Email','Email','required|valid_email');
            $this->form_validation->set_rules('Password','Password','required|matches[PasswordConfirm]');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><strong>Error : </strong>', '</div>');

            if($this->form_validation->run()) {

                $data['FullName'] = $this->input->post('FullName');
                $data['Email'] = $this->input->post('Email');
                $data['Password'] = md5($this->input->post('Password'));
                $data['Created'] = date('Y-m-d H:i:s');
                $data['Group'] = 'user';
                $data['Status'] = $this->input->post('Status');

                $this->db->insert('user', $data);
                $this->template->set_alert('success','<strong>Success : </strong> User added successfully');
                redirect(site_url('/admin/dashboard'));
            }
        }

        $this->template->view('admin/user-form',array('title' => 'Add User'));
    }


    public function edit($id){
        $user = $this->db->get_where('user',array('ID' => $id));

        if($user->num_rows() <= 0 ){
            $this->template->set_alert('danger','<strong>Error : </strong> User not found');
            redirect(site_url('/admin/dashboard'));
        }

        $this->template->formData = $user->row_array();
        $this->load->library('form_validation');

        if($this->input->post('submit')) {
            $this->form_validation->set_rules('FullName','Full Name','required');
            $this->form_validation->set_rules('Email','Email','required|valid_email');

            if($this->input->post('Password') != FALSE) {
                $this->form_validation->set_rules('Password','Password','required|matches[PasswordConfirm]');
                $data['Password'] = md5($this->input->post('Password'));
            }

            $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><strong>Error : </strong>', '</div>');

            if($this->form_validation->run()) {

                $data['FullName'] = $this->input->post('FullName');
                $data['Email'] = $this->input->post('Email');
                $data['Modified'] = date('Y-m-d H:i:s');
                $data['Status'] = $this->input->post('Status');

                $this->db->where('ID', $id);
                $this->db->update('user', $data);
                $this->template->set_alert('success','<strong>Success : </strong> User Updated successfully');
                redirect(site_url('/admin/dashboard'));
            }
        }

        $this->template->view('admin/user-form',array('title' => 'Edit Form'));


    }
}

/*
 * End File dashboard.php 
 */

 