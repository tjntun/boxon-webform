<?php
 
class lagerhold_sjaelland extends CI_Controller {

    private $formType = 'lagerhold_sjaelland';

    public function __construct(){
        parent::__construct();

        if(!$this->user->is_logged_in()) {
            $this->template->set_alert('danger','You must be logged in to use that page');
            redirect('/home');
        }
    }

    public function index()
    {
        $this->load->library('pagination');

        $this->db->start_cache();
        $this->db->select('*')->from('user_form')->where(array('UserID' => $this->user->user['ID'], 'FormType' => $this->formType))->order_by('ID','DESC');
        $this->db->stop_cache();

        $per_page = 10;

        $total_rows = $this->db->count_all_results();

        $config['base_url'] = site_url('/forms/');
        $config['total_rows'] = intval($total_rows);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '</li>';
        $config['first_tag_close'] = '<li>';

        $current_page = $this->uri->segment(5) == false ? 0 : intval($this->uri->segment(5));

        $this->pagination->initialize($config);
        $data = $this->db->get(NULL, $per_page, $current_page)->result_array();
        $this->template->view('forms',array('forms' => $data, 'formType' => $this->formType));
    }

    public function add(){
        $this->load->library('form_validation');
        
        if($this->input->post('submit') != FALSE) {
            $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

            if(true) {

                unset($_POST['submit']);

                $data['UserID'] = $this->user->user['ID'];
                $data['Created'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);
                $data['FormType'] = $this->formType;

                $this->db->insert('user_form',$data);

                $this->template->set_alert('success','<strong>Success :</strong>Form Added Successfully');
                redirect(site_url('/'.$this->formType));

            }
        }

        $this->template->view('forms/'.$this->formType,array('formType'=>$this->formType,  'action' => 'add'));
    }

    public function edit($formID = ''){
        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/'.$this->formType));
        }

        $this->load->library('form_validation');

        if($this->input->post('submit') != FALSE) {

            $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

            if(true) {

                unset($_POST['submit']);

                $data['Modified'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);

                $this->db->where('ID', $formID);
                $this->db->update('user_form',$data);

                $this->template->set_alert('success','<strong>Success :</strong> Form Edited Successfully');
                redirect(site_url('/'. $this->formType .'/edit/'.$formID));
            }
        }


        $form = $form->row_array();
        $this->template->formData = json_decode($form['FormData'],true);

        $this->template->view('forms/'.$this->formType,array('form' => $form, 'formType'=>$this->formType, 'action' => 'edit'));
    }

    public function delete($formID = ''){
        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/'.$this->formType));
        } else {
            $this->db->where('ID',$formID);
            $this->db->delete('user_form');
            $this->template->set_alert('success', '<strong>Success :</strong> Form Deleted successfully.');
            redirect(site_url('/'.$this->formType));
        }
    }

    public function generatePdf($formID = ''){

        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'],true);

        $this->load->helper(array('dompdf', 'file'));
        $html = $this->load->view('forms/'.$this->formType.'_pdf', null, true);
        pdf_create($html, $this->formType);
    }
}