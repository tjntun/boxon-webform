<?php

class price_request_mondi extends CI_Controller
{
    private $formType = 'price_request_mondi';

    public function __construct(){
        parent::__construct();

        if(!$this->user->is_logged_in()) {
            $this->template->set_alert('danger','You must be logged in to use that page');
            redirect('/home');
        }
    }

    public function index()
    {
        $this->load->library('pagination');

        $this->db->start_cache();
        $this->db->select('*')->from('user_form')->where(array('UserID' => $this->user->user['ID'], 'FormType' => $this->formType))->order_by('ID','DESC');
        $this->db->stop_cache();

        $per_page = 10;

        $total_rows = $this->db->count_all_results();

        $config['base_url'] = site_url('/forms/');
        $config['total_rows'] = intval($total_rows);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '</li>';
        $config['first_tag_close'] = '<li>';

        $current_page = $this->uri->segment(5) == false ? 0 : intval($this->uri->segment(5));

        $this->pagination->initialize($config);
        $data = $this->db->get(NULL, $per_page, $current_page)->result_array();
        $this->template->view('forms',array('forms' => $data, 'formType' => $this->formType));
    }

    public function add(){
        $this->load->library('form_validation');
        
        if($this->input->post('submit') != FALSE) {
            $this->form_validation->set_rules('requestdate','Request Date','required|trim');
            $this->form_validation->set_rules('prreference','Preference','required|trim');
            $this->form_validation->set_rules('reference','Reference','required|trim');
            $this->form_validation->set_rules('fefco','FeFco','required|trim');
            $this->form_validation->set_rules('quantity','Quantity','required|trim');
            $this->form_validation->set_rules('length','Length','required|trim');
            $this->form_validation->set_rules('width','Width','required|trim');
            $this->form_validation->set_rules('height','Height','required|trim');
            $this->form_validation->set_rules('print','Print','required|trim');
            $this->form_validation->set_rules('coverage','Coverage','required|trim');
            $this->form_validation->set_rules('assembling','Assembling','required|trim');
            $this->form_validation->set_rules('quantity1','Quantity One','required|trim');
            $this->form_validation->set_rules('quantity2','Quantity Two','required|trim');
            $this->form_validation->set_rules('quantity3','Quantity Three','required|trim');
            $this->form_validation->set_rules('quantity4','Quantity Four','required|trim');
            $this->form_validation->set_rules('bundled','Bundled','required|trim');
            $this->form_validation->set_rules('peicesbundle','Peices bundle','required|trim');
            $this->form_validation->set_rules('strecthfilmonpallets','Strecth fil monpallets','required|trim');
            $this->form_validation->set_rules('quantityperpallet','Quantity Perpallet','required|trim');
            $this->form_validation->set_rules('maxpalletdimension','Max Palletdimension','required|trim');
            $this->form_validation->set_rules('length1','Length One','required|trim');
            $this->form_validation->set_rules('width1','Width One','required|trim');
            $this->form_validation->set_rules('height1','Height One','required|trim');
            $this->form_validation->set_rules('pallet','Pallet','required|trim');

            $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

            if($this->form_validation->run() == true) {

                unset($_POST['submit']);

                $data['UserID'] = $this->user->user['ID'];
                $data['Created'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);
                $data['FormType'] = $this->formType;

                $this->db->insert('user_form',$data);

                $this->template->set_alert('success','<strong>Success :</strong>Form Added Successfully');
                redirect(site_url('/'.$this->formType));

            }
        }

        $this->template->view('forms/'.$this->formType);
    }

    public function edit($formID = ''){
        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $this->load->library('form_validation');

        if($this->input->post('submit') != FALSE) {

            $this->form_validation->set_rules('requestdate','Request Date','required|trim');
            $this->form_validation->set_rules('prreference','Preference','required|trim');
            $this->form_validation->set_rules('reference','Reference','required|trim');
            $this->form_validation->set_rules('fefco','FeFco','required|trim');
            $this->form_validation->set_rules('quantity','Quantity','required|trim');
            $this->form_validation->set_rules('length','Length','required|trim');
            $this->form_validation->set_rules('width','Width','required|trim');
            $this->form_validation->set_rules('height','Height','required|trim');
            $this->form_validation->set_rules('print','Print','required|trim');
            $this->form_validation->set_rules('coverage','Coverage','required|trim');
            $this->form_validation->set_rules('assembling','Assembling','required|trim');
            $this->form_validation->set_rules('quantity1','Quantity One','required|trim');
            $this->form_validation->set_rules('quantity2','Quantity Two','required|trim');
            $this->form_validation->set_rules('quantity3','Quantity Three','required|trim');
            $this->form_validation->set_rules('quantity4','Quantity Four','required|trim');
            $this->form_validation->set_rules('bundled','Bundled','required|trim');
            $this->form_validation->set_rules('peicesbundle','Peices bundle','required|trim');
            $this->form_validation->set_rules('strecthfilmonpallets','Strecth fil monpallets','required|trim');
            $this->form_validation->set_rules('quantityperpallet','Quantity Perpallet','required|trim');
            $this->form_validation->set_rules('maxpalletdimension','Max Palletdimension','required|trim');
            $this->form_validation->set_rules('length1','Length One','required|trim');
            $this->form_validation->set_rules('width1','Width One','required|trim');
            $this->form_validation->set_rules('height1','Height One','required|trim');
            $this->form_validation->set_rules('pallet','Pallet','required|trim');

            $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

            if($this->form_validation->run() == true) {

                unset($_POST['submit']);

                $data['Modified'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);

                $this->db->where('ID', $formID);
                $this->db->update('user_form',$data);

                $this->template->set_alert('success','<strong>Success :</strong> Form Edited Successfully');
                redirect(site_url('/'. $this->formType .'/edit/'.$formID));
            }
        }


        $form = $form->row_array();
        $this->template->formData = json_decode($form['FormData'],true);

        $this->template->view('forms/'.$this->formType,array('form' => $form, 'action' => 'edit'));
    }

    public function generatePdf($formID = ''){

        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/'.$this->formType));
        }

        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'],true);

        $this->load->helper(array('dompdf', 'file'));
        $html = $this->load->view('forms/'.$this->formType.'_pdf', null, true);
        pdf_create($html, 'Price_werner');
    }

    public function viewPdf($formID = '')
    {
        $form = $this->db->get_where('user_form',array('ID' => $formID));
        $form = $form->row_array();
        
        $this->template->formData = json_decode($form['FormData'],true);
        $this->template->view('forms/'.$this->formType.'_pdf',array('formType' => $this->formType));
    }

    public function email($formID = ''){

        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/'.$this->formType));
        }

        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'],true);

        $this->load->library('form_validation');

        if($this->input->post('submit') != FALSE) {
            $this->form_validation->set_rules('emails','Emails','required|valid_emails');
            $this->form_validation->set_rules('subject','Subject','required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            if($this->form_validation->run() == true) {
                $this->load->library('email');

                $emails = explode(',',$this->input->post('emails'));
                $this->load->helper(array('dompdf', 'file'));

                $html = $this->load->view('forms/'. $this->formType .'_pdf', null, true);
                $pdf = pdf_create($html, 'Price_werner',false);
                $fileName = 'pdf-'.time('now').'.pdf';
                write_file('./uploads/'.$fileName,$pdf);


                foreach($emails as $email) {
                    $this->email->clear();
                    $this->email->from($this->config->item('EMAIL_FROM'), 'PDF Generator');
                    $this->email->to($email);

                    $this->email->subject($this->input->post('subject'));
                    $this->email->message($this->input->post('message'));


                    $this->email->attach('./uploads/'.$fileName);
                    $this->email->send();
                }

                $this->template->set_alert('success','<strong>Success : </strong> Email sent successfully');
                unlink(APPPATH.'/uploads/'.$fileName);
                redirect(site_url('/'.$this->formType));
            }
        }

        $this->template->view('email');
    }

    public function delete($formID = ''){
        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/'.$this->formType));
        } else {
            $this->db->where('ID',$formID);
            $this->db->delete('user_form');
            $this->template->set_alert('success', '<strong>Success :</strong> Form Deleted successfully.');
            redirect(site_url('/'.$this->formType));
        }
    }

}