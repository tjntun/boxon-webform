<?php

/*
 * Created By : Touqeer Shafi
 * Email : touqeer.shafi@gmail.com
 * Date : 7/29/14
 * File : home.php 
 */

class home extends CI_Controller
{
     public function __construct(){
        parent::__construct();

        if(!$this->user->is_logged_in('user')){
            // redirect('/');
        }
    }

    public function index() {

        if ($this->user->is_logged_in('user')) {
            redirect('/forms/add');
        }else{

        $this->load->library('form_validation');
        $this->template->view('home');
        }

    }

    public function logout()
    {
        if ($this->user->is_logged_in('user')) {
            $this->user->logout('user');
            redirect('/');
        }
    }

    public function my_info()
    {
        $this->load->library('form_validation');
        $user = $this->db->get_where('user',array('ID' => $this->user->user['ID']));
        if($user->num_rows() <= 0 ){
            $this->template->set_alert('danger','<strong>Error : </strong> User not found');
            redirect(site_url('/'));
        }

        $this->template->formData = $user->row_array();

        if($this->input->post('submit') != FALSE){
            $this->form_validation->set_rules('FullName','Full Name','required');
            $this->form_validation->set_rules('Email','Email','required|valid_email');
            $this->form_validation->set_rules('Password','Password','matches[PasswordConfirm]');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><strong>Error : </strong>', '</div>');

            if($this->form_validation->run()) {
                // var_dump($this->user->user['ID']);die;
                // var_dump($this->input->post());die;

                $data['FullName'] = $this->input->post('FullName');
                $data['Email'] = $this->input->post('Email');
                if ($this->input->post('Password') != '') {
                    $data['Password'] = md5($this->input->post('Password'));
                }
                $data['Modified'] = date('Y-m-d H:i:s');
                $this->db->get('user');
                $this->db->where("ID",$this->user->user['ID']);
                $this->db->update('user', $data);
                $this->template->set_alert('success','<strong>Success : </strong> User updated successfully');
                redirect(site_url('/home/my_info'));
            }
        }

        $this->template->view('my-info');
    }


}

/*
 * End File home.php 
 */

 