<?php

/**
 * Created by Touqeer Shafi.
 * File : ordre_og.php
 * Date: 8/8/14
 * Time: 10:00 AM
 */
class ordre_og extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->user->is_logged_in()) {
            $this->template->set_alert('danger', 'You must be logged in to use that page');
            redirect('/home');
        }

    }


    public function add()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

        if ($this->input->post('submit') != FALSE) {

            $this->form_validation->set_rules('firmanavn', ' ', 'required|trim');
            $this->form_validation->set_rules('CVR_nr', ' ', 'required|trim');
            $this->form_validation->set_rules('saelger', ' ', 'required|trim');
            $this->form_validation->set_rules('kontaktperson', ' ', 'required|trim');
            $this->form_validation->set_rules('mail', ' ', 'required|trim');
            $this->form_validation->set_rules('telefonnummer', ' ', 'required|trim');
            $this->form_validation->set_rules('fakturamail', ' ', 'required|trim');
            $this->form_validation->set_rules('faktureringsadresse', ' ', 'required|trim');
            $this->form_validation->set_rules('leveringsadresse', ' ', 'required|trim');
            $this->form_validation->set_rules('dato', ' ', 'required|trim');
            $this->form_validation->set_rules('leveringsdato', ' ', 'required|trim');
            $this->form_validation->set_rules('id_ref', ' ', 'required|trim');
            $this->form_validation->set_rules('fefco', ' ', 'required|trim');
            $this->form_validation->set_rules('Prove_vedlagt', ' ', 'required|trim');
            $this->form_validation->set_rules('kvalitet', ' ', 'required|trim');
            $this->form_validation->set_rules('flute', ' ', 'required|trim');
            $this->form_validation->set_rules('mal', ' ', 'required|trim');
            $this->form_validation->set_rules('oplag', ' ', 'required|trim');
            $this->form_validation->set_rules('targetpris', ' ', 'required|trim');
            $this->form_validation->set_rules('Salgspris', ' ', 'required|trim');
            $this->form_validation->set_rules('Antal_pr_bundt', ' ', 'required|trim');
            $this->form_validation->set_rules('Antal_pr_palle', ' ', 'required|trim');
            $this->form_validation->set_rules('Max_pallemal', ' ', 'required|trim');
            $this->form_validation->set_rules('Antal_farver', ' ', 'required|trim');
            $this->form_validation->set_rules('tegnings_nr', ' ', 'required|trim');
            $this->form_validation->set_rules('Procent_deakning', ' ', 'required|trim');
            $this->form_validation->set_rules('Pantone_nummer', ' ', 'required|trim');
            $this->form_validation->set_rules('Kundemail', ' ', 'required|trim');
            $this->form_validation->set_rules('Standardvarer', ' ', 'required|trim');

            if ($this->form_validation->run()) {

                unset($_POST['submit']);

                $data['UserID'] = $this->user->user['ID'];
                $data['Created'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);
                $data['FormType'] = 'ordre_og';

                $this->db->insert('user_form', $data);

                $this->template->set_alert('success', '<strong>Success :</strong> Perice Wener Form Added Successfully');
                redirect(site_url('/forms'));

            } else {
                $this->template->set_alert('danger', '<strong>Warning :</strong> Validation error occured');
            }


        }


        $this->template->view('forms/ordre_og');

    }

    public function generatePdf($formID = '')
    {

        $this->load->library('form_validation');

        $form = $this->db->get_where('user_form', array('ID' => $formID));

        if ($form->num_rows() <= 0) {
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'], true);

        $this->load->helper(array('dompdf', 'file'));
        $html = $this->load->view('forms/ordre_og_pdf', null, true);
        pdf_create($html, 'ordre_og');

    }

    public function email($formID = '')
    {

        $form = $this->db->get_where('user_form', array('ID' => $formID));

        if ($form->num_rows() <= 0) {
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'], true);

        $this->load->library('form_validation');


        if ($this->input->post('submit') != FALSE) {
            $this->form_validation->set_rules('emails', 'Emails', 'required|valid_emails');
            $this->form_validation->set_rules('subject', 'Subject', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            if ($this->form_validation->run() == true) {
                $this->load->library('email');

                $emails = explode(',', $this->input->post('emails'));
                $this->load->helper(array('dompdf', 'file'));

                $html = $this->load->view('forms/ordre_og_pdf', null, true);
                $pdf = pdf_create($html, 'ordre_og_pdf', false);
                $fileName = 'pdf-' . time('now') . '.pdf';
                write_file('./uploads/' . $fileName, $pdf);


                foreach ($emails as $email) {
                    $this->email->clear();
                    $this->email->from($this->config->item('EMAIL_FROM'), 'PDF Generator');
                    $this->email->to($email);

                    $this->email->subject($this->input->post('subject'));
                    $this->email->message($this->input->post('message'));


                    $this->email->attach('./uploads/' . $fileName);
                    $this->email->send();
                }

                $this->template->set_alert('success', '<strong>Success : </strong> Email sent successfully');
                @unlink(APPPATH . '/uploads/' . $fileName);
                redirect(site_url('/forms'));

            }

        }

        $this->template->view('email');
    }


    public function edit($formID = '')
    {
        $form = $this->db->get_where('user_form', array('ID' => $formID));

        if ($form->num_rows() <= 0) {
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        }

        $this->load->library('form_validation');

        if ($this->input->post('submit') != FALSE) {

            $this->form_validation->set_rules('firmanavn', ' ', 'required|trim');
            $this->form_validation->set_rules('CVR_nr', ' ', 'required|trim');
            $this->form_validation->set_rules('saelger', ' ', 'required|trim');
            $this->form_validation->set_rules('kontaktperson', ' ', 'required|trim');
            $this->form_validation->set_rules('mail', ' ', 'required|trim');
            $this->form_validation->set_rules('telefonnummer', ' ', 'required|trim');
            $this->form_validation->set_rules('fakturamail', ' ', 'required|trim');
            $this->form_validation->set_rules('faktureringsadresse', ' ', 'required|trim');
            $this->form_validation->set_rules('leveringsadresse', ' ', 'required|trim');
            $this->form_validation->set_rules('dato', ' ', 'required|trim');
            $this->form_validation->set_rules('leveringsdato', ' ', 'required|trim');
            $this->form_validation->set_rules('id_ref', ' ', 'required|trim');
            $this->form_validation->set_rules('fefco', ' ', 'required|trim');
            $this->form_validation->set_rules('Prove_vedlagt', ' ', 'required|trim');
            $this->form_validation->set_rules('kvalitet', ' ', 'required|trim');
            $this->form_validation->set_rules('flute', ' ', 'required|trim');
            $this->form_validation->set_rules('mal', ' ', 'required|trim');
            $this->form_validation->set_rules('oplag', ' ', 'required|trim');
            $this->form_validation->set_rules('targetpris', ' ', 'required|trim');
            $this->form_validation->set_rules('Salgspris', ' ', 'required|trim');
            $this->form_validation->set_rules('Antal_pr_bundt', ' ', 'required|trim');
            $this->form_validation->set_rules('Antal_pr_palle', ' ', 'required|trim');
            $this->form_validation->set_rules('Max_pallemal', ' ', 'required|trim');
            $this->form_validation->set_rules('Antal_farver', ' ', 'required|trim');
            $this->form_validation->set_rules('tegnings_nr', ' ', 'required|trim');
            $this->form_validation->set_rules('Procent_deakning', ' ', 'required|trim');
            $this->form_validation->set_rules('Pantone_nummer', ' ', 'required|trim');
            $this->form_validation->set_rules('Kundemail', ' ', 'required|trim');
            $this->form_validation->set_rules('Standardvarer', ' ', 'required|trim');


            $this->form_validation->set_error_delimiters('<span class="hint-error">', '</span>');

            if ($this->form_validation->run() == true) {

                unset($_POST['submit']);

                $data['Modified'] = date('Y-m-d H:i:s');
                $data['FormData'] = json_encode($_POST);

                $this->db->where('ID', $formID);
                $this->db->update('user_form', $data);

                $this->template->set_alert('success', '<strong>Success :</strong> Form Edited Successfully');
                redirect(site_url('/forms'));

            } else {
                echo validation_errors();
                die();
            }
        }


        $form = $form->row_array();

        $this->template->formData = json_decode($form['FormData'], true);

        $this->template->view('forms/ordre_og', array('form' => $form));

    }


}

/**
 * End File : ordre_og.php
 */ 