<?php
/**
 * Created by Touqeer Shafi.
 * File : sjaelland.php
 * Date: 8/19/14
 * Time: 10:04 AM
 */
 

class sjaelland extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }


    public function index() {
        $this->template->view('forms/sjaelland');
    }

}
 
 
 /**
  * End File : sjaelland.php
 */ 