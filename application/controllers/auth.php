<?php

/*
 * Created By : Touqeer Shafi
 * Email : touqeer.shafi@gmail.com
 * Date : 7/29/14
 * File : user.php 
 */

class auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login()
    {

        $this->load->library('form_validation');



        if ($this->input->post('submit') != FALSE) {

            $this->form_validation->set_rules('user_email', 'User Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('user_pass', 'User Password', 'trim|required');

            if ($this->form_validation->run() == true) {

                if ($this->user->auth($this->input->post('user_email'), $this->input->post('user_pass'))) {
                    $this->template->set_alert('success', 'You are now logged in');
                    redirect('/forms/add');
                } else {
                    $this->template->set_alert('info', 'Username of password incorrect');
                }

            };



        } else {
            $this->template->set_alert('error', 'Validation error');
        }



        $this->template->view('home');

    }

}

/*
 * End File user.php 
 */

 