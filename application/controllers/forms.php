<?php

class Forms extends CI_Controller {

    public function __construct(){
        parent::__construct();

        if(!$this->user->is_logged_in('user')){
            $this->template->set_alert('info','You must be logged in to see that page');
            redirect(site_url('/home'));
        }

    }

    public function price_request_mondi(){

        $this->load->library('pagination');

        $this->db->start_cache();
        $this->db->select('*')->from('user_form')->where(array('UserID' => $this->user->user['ID'], 'FormType' => 'price_request_mondi'))->order_by('ID','DESC');
        $this->db->stop_cache();

        $per_page = 10;

        $total_rows = $this->db->count_all_results();

        $config['base_url'] = site_url('/forms/');
        $config['total_rows'] = intval($total_rows);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '</li>';
        $config['first_tag_close'] = '<li>';

        $current_page = $this->uri->segment(5) == false ? 0 : intval($this->uri->segment(5));


        $this->pagination->initialize($config);
        $data['forms'] = $this->db->get(NULL, $per_page, $current_page)->result_array();

        $this->template->view('forms', $data);
    }


    public function add(){
        $this->template->view('form-add');
    }

    public function delete($formID = ''){
        $form = $this->db->get_where('user_form',array('ID' => $formID));

        if($form->num_rows() <= 0 ){
            $this->template->set_alert('danger', 'Form Not found');
            redirect(site_url('/forms'));
        } else {
            $this->db->where('ID',$formID);
            $this->db->delete('user_form');
            $this->template->set_alert('success', '<strong>Success :</strong> Form Deleted successfully.');
            redirect(site_url('/forms'));
        }
    }

}
 
 
/*
 * End File forms.php 
 */

 