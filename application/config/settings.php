<?php
/*
 * Created By : Touqeer Shafi
 * Email : touqeer.shafi@gmail.com
 * Date : 7/29/14
 * File : settings.php
 */

$config['SITE_TITLE'] = 'Form Application';
$config['EMAIL_FROM'] = 'admin@webforms.com';

/*
 * End File settings.php
 */

