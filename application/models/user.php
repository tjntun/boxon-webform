<?php

/*
 * Created By : Touqeer Shafi
 * Email : touqeer.shafi@gmail.com
 * Date : 7/29/14
 * File : user.php 
 */

class user extends CI_Model
{

    public $user = array();
    public $admin = array();

    public function __construct()
    {
        $user = $this->session->userdata('user');
        $admin = $this->session->userdata('admin');

        if ($user != FALSE) {
            $this->user = $user;
        }

        if ($admin != FALSE) {
            $this->admin = $admin;
        }
    }

    public function logout($group = 'user')
    {
        $this->session->unset_userdata($group);
    }

    public function set_user_data($user, $group = 'user')
    {

        $user['logged_in'] = true;
        $this->session->set_userdata($group, $user);

        return true;
    }

    public function is_logged_in($group = 'user')
    {

        $groupUser = $this->{$group};

        if (isset($groupUser['logged_in'])) {
            return true;
        }

        return false;
    }

    public function auth($email, $pass, $group = 'user')
    {

        $user = $this->db->get_where('user', array('Email' => $email, 'status' => 'active', 'group' => $group))->row_array();

        if (count($user) <= 0) {
            return false;
        }

        if (md5($pass) == $user['Password']) {
            $this->set_user_data($user, $group);
            $this->db->where('ID', $user['ID'])->update('user',array('LastLogin' => date("Y-m-d H:i:s")));
            return true;
        }

        return false;

    }

}

/*
 * End File user.php 
 */

 