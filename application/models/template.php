<?php

/*
 * Created By : Touqeer Shafi
 * Email : touqeer.shafi@gmail.com
 * Date : 7/29/14
 * File : template.php
 */

class template extends CI_Model
{

    public $section = true;
    public $google = array();

    public function __construct()
    {
        parent::__construct();

        if ($this->uri->segment(1) == 'admin') {
            $this->section = false;
        }

    }


    public function _header()
    {

        if ($this->section)
            $this->load->view('header');
        else
            $this->load->view('admin/header');

    }

    public function _footer()
    {
        if ($this->section)
            $this->load->view('footer');
        else
            $this->load->view('admin/footer');
    }

    public function view($view, $data = array())
    {
        $this->_header();
        $this->load->view($view, $data);
        $this->_footer();
    }

    public function set_alert($type, $message)
    {
        $this->session->set_flashdata('message', array($type => $message));
    }


    public function setFieldValue($key){

        if(isset($this->formData) && isset($this->formData[$key])){
            return $this->formData[$key];
        }
    }

    public function checked($key, $value = 'YES'){


        if(isset($_POST[$key]) && $_POST[$key] == $value){
            $value2 = $_POST[$key];
        } else {
            $value2 = $this->setFieldValue($key);
        }


        if($value == $value2){
            return ' checked="checked" ';
        }


    }

}

/*
 * End File template.php
 */

