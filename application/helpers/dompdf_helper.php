<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by Touqeer Shafi.
 * File : dompdf_helper.php
 * Date: 8/4/14
 * Time: 11:38 AM
 */


function pdf_create($html, $filename='', $stream=TRUE)
{
    require_once("dompdf/dompdf_config.inc.php");

    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    if ($stream) {
        $dompdf->stream($filename.".pdf");
    } else {
        return $dompdf->output();
    }
}
 
 
 /**
  * End File : dompdf_helper.php
 */ 